#include <iostream>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <random>
#include <cstdio>

#include "../include/test.h"
#include "../include/json.h"

//#define CACHE_FILE

class config {
public:
    config() : buffer_size(0), repetition(0), rand_reads(0), rnd_read_buf_size(0), seed(0) {}
    bool init(Json::Value& root, std::string& error_message)
    {
        try {
            buffer_size = root.get("buffer_size", 50 * 1024).asUInt(); //v Bytech
        }
        catch(std::runtime_error& e) {
            error_message = "'buffer_size' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            repetition = root.get("repetition", 1000).asUInt(); //pocet opakovani zapisu bufferu do souboru za sebou
        }
        catch(std::runtime_error& e) {
            error_message = "'repetition' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            rand_reads = root.get("rand_reads", 50 * 1024 * 1000 / 4).asUInt(); // pocet cteni v random testu,
            //defaultni hodnota je buffer_size * repetition / sizeof(unsigned)
        }
        catch(std::runtime_error& e) {
            error_message = "'rand_reads' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            rnd_read_buf_size = root.get("rnd_read_buf_size", 4).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'rnd_read_buf_size' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            seed = root.get("seed", 42).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'seed' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            file = root.get("file", "./tmp_card_tests").asString();
        }
        catch(std::runtime_error& e) {
            error_message = "'file' parameter read failed. String expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        return true;
    }
    size_t buffer_size;
    size_t repetition;
    size_t rand_reads;
    size_t rnd_read_buf_size;
    unsigned seed;
    std::string file;
};


namespace {
    bool verbose;
    config params;
}

#ifndef CACHE_FILE
bool flush_cache() {
    if (geteuid() != 0) {
        return false; // potrebujeme administratorska opravneni
    }

    sync(); //propsat buffery na uloziste

    // vymazat diskove cache
    FILE *fp = fopen("/proc/sys/vm/drop_caches", "w");
    if (fp == NULL) {
        return false;
    }
    fprintf(fp, "3\n");
    fclose(fp);
    return true;
}
#else
bool flush_cache() {
    int child_pid;

    child_pid = fork();
    switch(child_pid) {
    case -1:
        // fork failed
        return false;
        break;
    case 0:
        // child process
        int filedes;
        filedes = open("/dev/null", O_WRONLY);
        dup2(filedes, 1); // vystup drop_caches nebudeme zobrazovat
        execlp("/mnt/home/stefan/drop_caches", "drop_caches", (char *)NULL);
        break;
    default:
        // parent process
        wait(0);
        break;
    }
    return true;
}
#endif

//-----sequential writing-----
class seq_write : public test {
public:
    seq_write() : file_descriptor_(-1), buffer_(nullptr), written_(0) {}
    virtual std::string get_name()
    {
        return std::string("seq_write");
    }
    virtual std::string get_info()
    {
        return std::string("sequential writing to file");
    }
    virtual bool prepare_environment()
    {
        if(verbose) {
            std::cout << "  buffer size: " << params.buffer_size << "B" << std::endl;
            std::cout << "  repetition: " << params.repetition << std::endl;
            std::cout << "  file size: " << params.buffer_size * params.repetition << "B" << std::endl;
            std::cout << "  seed: " << params.seed << std::endl;
            std::cout << "  file: " << params.file << std::endl;
        }
        written_ = 0;
        buffer_ = new unsigned char[params.buffer_size];
        std::default_random_engine generator(params.seed);
        std::uniform_int_distribution<char> dist(0,255);
        auto my_rand = std::bind(dist, generator);
        for(size_t i = 0; i < params.buffer_size; i++) {
            buffer_[i] = my_rand();
        }
        file_descriptor_ = open(params.file.c_str(), O_WRONLY | O_CREAT | O_TRUNC | O_SYNC, 0770);
        if(file_descriptor_ == -1) {
            last_error_ = "Cannot open file for writing.";
            return false;
        }
        return true;
    }
    virtual void run_test()
    {
        for(size_t i = 0; i < params.repetition; i++) {
            written_ += write(file_descriptor_, &buffer_, params.buffer_size);
        }
        return;
    }
    virtual bool check_result()
    {
        if(file_descriptor_ != -1) {
            close(file_descriptor_);
        }
        if(written_ == params.buffer_size * params.repetition) {
            return true;
        }
        else {
            last_error_ = "Written size is wrong.";
            return false;
        }
    }
    virtual void evaluate(double time)
    {
        std::cout << "Evaluated:" << std::endl << "  write speed: " << params.buffer_size * params.repetition / 1024.0 / 1024.0 / time << " MB/s" << std::endl;
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        delete[] buffer_;
        buffer_ = nullptr;
        remove(params.file.c_str()); // smaze docasny soubor
        return true;
    }
    ~seq_write()
    {
        delete[] buffer_;
    }
private:
    std::string last_error_;
    int file_descriptor_;
    unsigned char* buffer_;
    size_t written_;
};


//-----sequential reading-----
class seq_read : public test {
public:
    seq_read() : file_descriptor_(-1), buffer_(nullptr), read_(0) {}
    virtual std::string get_name()
    {
        return std::string("seq_read");
    }
    virtual std::string get_info()
    {
        return std::string("sequential reading from file");
    }
    virtual bool prepare_environment()
    {
        if(verbose) {
            std::cout << "  buffer size: " << params.buffer_size << "B" << std::endl;
            std::cout << "  repetition: " << params.repetition << std::endl;
            std::cout << "  file size: " << params.buffer_size * params.repetition << "B" << std::endl;
            std::cout << "  seed: " << params.seed << std::endl;
            std::cout << "  file: " << params.file << std::endl;
        }
        read_ = 0;
        buffer_ = new unsigned char[params.buffer_size];
        std::default_random_engine generator(params.seed);
        std::uniform_int_distribution<char> dist(0,255);
        auto my_rand = std::bind(dist, generator);
        for(size_t i = 0; i < params.buffer_size; i++) {
            buffer_[i] = my_rand();
        }
        file_descriptor_ = open(params.file.c_str(), O_WRONLY | O_CREAT | O_TRUNC | O_SYNC, 0770); // posledni parametr jsou prava souboru
        if(file_descriptor_ == -1) {
            last_error_ = "Cannot prepare file for test - open for write problem.";
            return false;
        }
        size_t written = 0;
        for(size_t i = 0; i < params.repetition; i++) {
            written += write(file_descriptor_, buffer_, params.buffer_size);
        }
        if(written != params.buffer_size * params.repetition) {
            last_error_ = "Cannot prepare file for test - filling issue.";
            return false;
        }
        close(file_descriptor_);
        file_descriptor_ = open(params.file.c_str(), O_RDONLY);
        if(file_descriptor_ == -1) {
            last_error_ = "Cannot prepare file for test - open for read problem.";
            return false;
        }

        if(!flush_cache()) {
            last_error_ = "Cannot flush cache - missing administrator access.";
            return false;
        }
        return true;
    }
    virtual void run_test()
    {
        size_t read_size;
        while((read_size = read(file_descriptor_, buffer_, params.buffer_size)) > 0) {
            read_ += read_size;
        }
        return;
    }
    virtual bool check_result()
    {
        if(file_descriptor_ != -1) {
            close(file_descriptor_);
        }
        if(read_ == params.buffer_size * params.repetition) {
            return true;
        }
        else {
            last_error_ = "Read size is wrong.";
            return false;
        }
    }
    virtual void evaluate(double time)
    {
        std::cout << "Evaluated:" << std::endl << "  read speed: " << params.buffer_size * params.repetition / 1024.0 / 1024.0 / time << " MB/s" << std::endl;
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        delete[] buffer_;
        buffer_ = nullptr;
        remove(params.file.c_str()); // smaze docasny soubor
        return true;
    }
    ~seq_read()
    {
        delete[] buffer_;
    }
private:
    std::string last_error_;
    int file_descriptor_;
    unsigned char* buffer_;
    size_t read_;
};


//-----random reading-----
class rnd_read : public test {
public:
    rnd_read() : file_descriptor_(-1), buffer_(nullptr), read_(0), rnd_read_buffer_(nullptr) {}
    virtual std::string get_name()
    {
        return std::string("rnd_read");
    }
    virtual std::string get_info()
    {
        return std::string("random reading from file");
    }
    virtual bool prepare_environment()
    {
        if(verbose) {
            std::cout << "  file size: " << params.buffer_size * params.repetition << "B" << std::endl;
            std::cout << "  number of reads (rand reads): " << params.rand_reads << std::endl;
            std::cout << "  read size (rnd read buf size): " << params.rnd_read_buf_size << "B / each" << std::endl;
            std::cout << "  seed: " << params.seed << std::endl;
            std::cout << "  file: " << params.file << std::endl;
        }
        buffer_ = new unsigned char[params.buffer_size];
        std::default_random_engine generator(params.seed);
        std::uniform_int_distribution<char> dist(0,255);
        auto my_rand = std::bind(dist, generator);
        for(size_t i = 0; i < params.buffer_size; i++) {
            buffer_[i] = my_rand();
        }
        file_descriptor_ = open(params.file.c_str(), O_WRONLY | O_CREAT | O_TRUNC | O_SYNC, 0770); // posledni parametr jsou prava souboru
        if(file_descriptor_ == -1) {
            last_error_ = "Cannot prepare file for test - open for write problem.";
            return false;
        }
        size_t written = 0;
        for(size_t i = 0; i < params.repetition; i++) {
            written += write(file_descriptor_, buffer_, params.buffer_size);
        }
        if(written != params.buffer_size * params.repetition) {
            last_error_ = "Cannot prepare file for test - filling issue.";
            return false;
        }
        close(file_descriptor_);
        file_descriptor_ = open(params.file.c_str(), O_RDONLY);
        if(file_descriptor_ == -1) {
            last_error_ = "Cannot prepare file for test - open for read problem.";
            return false;
        }
        rnd_read_buffer_ = new unsigned char[params.rnd_read_buf_size];
        if(!flush_cache()) {
            last_error_ = "Cannot flush cache - missing administrator access.";
            return false;
        }
        return true;
    }
    virtual void run_test()
    {
        last_error_.clear();
        std::default_random_engine generator(params.seed);
        std::uniform_int_distribution<unsigned> dist(0, params.buffer_size * params.repetition - sizeof(rnd_read_buffer_));
        auto my_rand = std::bind(dist, generator);
        off_t offset;
        for(size_t i = 0; i < params.rand_reads; i++) {
            offset = my_rand();
            size_t x = pread(file_descriptor_, rnd_read_buffer_, params.rnd_read_buf_size, offset);
            if(x != params.rnd_read_buf_size) {
                last_error_ = "Reading test failure - read size is wrong.";
            }
        }
    }
    virtual bool check_result()
    {
        if(last_error_.empty()) {
            return true;
        }
        else {
            return false;
        }
    }
    virtual void evaluate(double time)
    {
        std::cout << "Evaluated:" << std::endl << "  read speed: " << params.rand_reads * params.rnd_read_buf_size / 1024.0 / 1024.0 / time << " MB/s" << std::endl;
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        delete[] buffer_;
        buffer_ = nullptr;
        delete[] rnd_read_buffer_;
        rnd_read_buffer_ = nullptr;
        remove(params.file.c_str()); // smaze docasny soubor
        return true;
    }
    ~rnd_read()
    {
        delete[] buffer_;
        delete[] rnd_read_buffer_;
    }
private:
    std::string last_error_;
    int file_descriptor_;
    unsigned char* buffer_;
    ssize_t read_;
    unsigned char* rnd_read_buffer_;
};


class card_tests : public test_suite {
public:
    card_tests(void)
    {
        tests.push_back(std::unique_ptr<test>(new seq_write));
        tests.push_back(std::unique_ptr<test>(new seq_read));
        tests.push_back(std::unique_ptr<test>(new rnd_read));
    }
    virtual std::string get_name() {return std::string("card");}
    virtual std::string get_info() {return std::string("disk read and write operations");}
    virtual void print_params()
    {
        std::cout << "   * Params:" << std::endl << "       buffer size: " << params.buffer_size << "B" << std::endl <<
                     "       repetition: " << params.repetition << std::endl << "       rand reads: " << params.rand_reads <<
                     std::endl << "       rnd read buf size: " << params.rnd_read_buf_size << std::endl << "       seed: " <<
                     params.seed << std::endl << "       file: " << params.file << std::endl;
    }
    virtual ~card_tests() {}
};


extern "C" test_suite* create(bool verbose_flag, Json::Value& root)
{
    verbose = verbose_flag;
    std::string error_message;
    if(!params.init(root, error_message)) {
        std::cout << "Config - " << error_message << std::endl;
        return nullptr;
    }
    return new card_tests();
}

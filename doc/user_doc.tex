\documentclass[12pt,a4paper,titlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage[T1]{fontenc}
\usepackage{a4wide}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage[hypcap]{caption}
\usepackage[affil-it]{authblk}
\author{Petr Stefan}
\affil{Matematicko-fyzikální fakulta Univerzity Karlovy v Praze}
\date{\today}
\title{\textbf{Program benchmark}\\\ \\Uživatelská dokumentace}
\begin{document}

\maketitle

%=====Úvod=====
\section{Úvod}
Benchmark\footnote{\url{http://en.wikipedia.org/wiki/Benchmark\_(computing)} -- v angličtině} je program na měření výkonu počítačů. Je navržen pro porovnávání různých výkonnostních parametrů miniaturního počítače Raspberry Pi (model B)\footnote{\url{http://cs.wikipedia.org/wiki/Raspberry\_Pi}} s jinými počítači. Obsahuje jedenáct modulů syntetických testů, přičmž každý modul může obsahovat více algoritmů či pouštět stejný algoritmus na různá data. Testovací algoritmy byly vybrány jak s ohledem na celkové změření výkonu jednotlivých hardwarových dílů, tak s přihlédnutím k použití více těchto jednotek ve výpočetním klastru. To znamená, že důležité jsou hlavně testy, které nějakým způsobem simulují typické operace prováděné na jednom výpočetním uzlu v průběhu nějakého paralelního výpočtu. Grafická jednotka nepodporuje standardní rozhraní OpenGL ani OpenCL, proto není do testování zahrnuta.

Vzhledem k omezenému výkonu počítače Raspberry Pi a povaze syntetického benchmarku je celý program včetně modulů napsán v programovacím jazyce C++ s využitím možností dle normy C++11. Některé přejaté kusy kódu jsou v jazyce C podle normy C99. To zaručuje na jedné straně přenositelnost, na straně druhé dostatečnou rychlost běhu přeloženého kódu bez ovlivňování záležitostmi typu \textit{garbage collectoru}, lepší správu omezené operační paměti bez nutnosti běhu jakéhokoli virtuálního stroje a v neposlední řadě i komfortní správu zdrojových kódů v moderním objektově orientovaném jazyce.

Hardwarová výbava počítače také zužuje výběr vhodného operačního systému. Pro Raspberry Pi byl zvolen nejrozšířenější a nejlépe podporovaný systém Raspbian. To je odnož distribuce GNU/Linux Debian optimalizovaná přímo na toto zařízení. Program by měl jít bez problému zkompilovat pro jakoukoli distribuci GNU/Linuxu a s minimálními úpravami by měl pracovat i na ostatních UNIX systémech.

%=====Distribuce zdrojových souborů=====
\section{Distribuce zdrojových souborů}
Zdrojové soubory se distribuují jako archiv \texttt{benchmark.tar.gz}. Rozbalení provedete příkazem:
\begin{verbatim}
$ tar -zxvf benchmark.tar.gz
\end{verbatim}
V aktuálním adresáři se vytvoří složka \texttt{benchmark}, která bude obsahovat zdrojové soubory ve struktuře popsané v sekci \ref{sec:adresarova_struktura}.

%=====Adresářová struktura=====
\section{Adresářová struktura}
\label{sec:adresarova_struktura}
Následuje krátký popis adresářové struktury, ve které se distribuují zdrojové soubory. Každý modul, knihovna i hlavní program obsahují též projektový soubor pro program QtCreator (chcete-li pro \texttt{qmake}). Tyto soubory mají příponu \texttt{.pro} a pokud nechcete zdrojové soubory upravovat v tomto programu, bude vám pro překlad stačit přiložený \texttt{makefile} a tyto soubory můžete ignorovat či odstranit.
\subsection{Hlavní program}
Hlavní program naleznete ve složce \texttt{benchmark}. Obsahuje zdrojový soubor \texttt{main.cpp} a projektový \texttt{benchmark.pro}. Je zodpovědný za načítání jednotlivých modulů, spouštění testů, měření doby běhu a interakci s uživatelem přes parametry z příkazové řádky. Po překladu vznikne spustitelný soubor \texttt{benchmark}.
\subsection{Moduly}
Moduly s testy jsou v adresářích \texttt{cache}, \texttt{card}, \texttt{crypt}, \texttt{graph}, \texttt{join}, \texttt{levenshtein}, \texttt{matrix}, \texttt{mem}, \texttt{net}, \texttt{sort} a \texttt{zlib}. Většinou obsahují projektový soubor shodného jména s nadřazeným adresářem a soubor \texttt{main.cpp}, výjimečně i další soubory. Po překladu vznikne z každého modulu sdílená knihovna se jménem modulu (stejné jako jméno adresáře), prefixem \texttt{bm\_} a příponou \texttt{.so}; například \texttt{bm\_cache.so}, \texttt{bm\_card.so} atd.  Více informací o jednotlivých modulech naleznete v sekci \ref{sec:popis_modulu}.
\subsection{Parser JsonCpp}
C++ parser formátu JSON \texttt{jsoncpp} je ve složce \texttt{jsoncpp\_lib}. Je zde také projektový soubor \texttt{jsoncpp\_lib.pro} a zdrojový kód v \texttt{main.cpp}. Je použit kód sloučený do jediného souboru, jak umožňuje skript \texttt{amalgamate.py} z repozitáře projektu\footnote{\url{https://github.com/open-source-parsers/jsoncpp}}. Odpovídající hlavičkový soubor je přesunut do složky \texttt{includes} (viz. \ref{subsec:sdilene_hlavickove_soubory}) místo původní podsložky \texttt{json}. Parser je překládán jako sdílená knihovna z důvodu použití jeho kódu ve všech modulech. Výsledný přeložený soubor se jmenuje \texttt{libjsoncpp.so} a je k jednotlivým modulům staticky přilinkován.
\subsection{Sdílené hlavičkové soubory}
\label{subsec:sdilene_hlavickove_soubory}
Hlavičkové soubory sdílené hlavní aplikací a jednotlivými moduly jsou umístěny ve složce \texttt{includes}. Jedná se o soubory \texttt{test.h} a \texttt{json.h}. První zmíněný obsahuje deklaraci abstraktního rozhraní třídy jednoho testu, třídy sady testů a také definici typu tovární funkce modulů. Druhý soubor je sloučená hlavička parseru \texttt{jsoncpp}.
\subsection{Pomocný program pro síťové testy}
V adresáři \texttt{net-echo} je zdrojový soubor \texttt{net-echo.cpp} pro pomocný program na měření síťových testů. Tento program naslouchá na cílovém stroji na určeném portu a příchozí pakety odesílá zpět. Překlad tohoto pomocného programu není zajištěn přiloženým \texttt{makefile}, podrobnější instrukce naleznete v sekci \ref{sec:sestaveni}.
\subsection{Dokumentace}
Zdrojové soubory uživatelské a programátorské dokumentace ve formátu \TeX \: jsou v adresáři \texttt{doc}. Doporučený způsob překladu je pomocí utility \texttt{pdflatex}.
\subsection{Přeložené binární soubory}
Přeložené binární soubory se ukládají do složky \texttt{bin} (makefile), případně \texttt{debug} (QtCreator). V případě neexistence cílového adresáře bude tento automaticky vytvořen.
\subsection{Makefile}
\texttt{Makefile} je skript pro automatický překlad zdrojových souborů utilitkou \texttt{make}\footnote{\url{http://cs.wikipedia.org/wiki/Make}}. Podrobnější informace jsou v sekci \ref{sec:sestaveni}.
\subsection{Konfigurační soubor}
Ukázkový konfigurační soubor se jmenuje \texttt{config.json}. Je ve standardním formátu JSON\footnote{\url{http://cs.wikipedia.org/wiki/JSON}} s možností psát komentáře. Podobně jako v jazyce C++ uvozujeme jednořádkové komentáře dvojicí lomítek \texttt{//}, víceřádkové začínáme \texttt{/*} a ukončujeme \texttt{*/}. Vysvětlení jednotlivých parametrů naleznete vždy u příslušného modulu v sekci \ref{sec:popis_modulu}.

%=====Sestavení=====
\section{Sestavení}
\label{sec:sestaveni}
Jak již bylo zmíněno v úvodu, \texttt{benchmark} je navržen pro operační systémy GNU/Linux, případně i pro jiné Unixové systémy. Základním předpokladem pro úspěšné sestavení je nainstalovaná sada překladačů GCC\footnote{\url{http://cs.wikipedia.org/wiki/GCC}} ve verzi alespoň 4.7, doporučená verze je 4.8 a vyšší. Starší verze kompilátoru nepodporují plně standard C++11, který aplikace využívá. GCC by mělo být dostupné v repozitářích všech známějších distribucí. Pokud používáte jiný překladač jazyka C++, musíte patřičným způsobem upravit \texttt{makefile}, případně kompilovat nějakým jiným způsobem.

Pro Raspberry Pi či jiné obdobné \uv{malé} systémy lze překládat i pomocí cross-com\-pi\-lá\-to\-rů a na cílovou platformu přenést pouze produkty překladu.
\subsection{Pomocí makefile}
Soubor \texttt{makefile} zajišťuje překlad hlavního programu, všech modulů i sdílené knihovny s~parserem \texttt{jsoncpp} ve správném pořadí operací a se správnými přepínači. Nedoporučuje se tento soubor jakkoli editovat, pokud si nejste jisti tím, co děláte. Tento \texttt{makefile} podporuje dva základní cíle -- \textit{all} a \textit{clean}. Cíl \textit {all} je výchozí, tudíž ho není nutné jako parametr uvádět, a znamená překlad všech souborů, jejichž zdrojový kód se od posledního překladu změnil. Cíl \textit{clean} smaže staré přeložené binární soubory spolu s jejich objektovými soubory (soubory s příponou \texttt{.o} v podadresářích složky \texttt{bin}) vzniklými při překladu. Samotný překlad se provede následujícím příkazem spuštěným v hlavním adresáři celého projektu:
\begin{verbatim}
$ make
\end{verbatim}
Vynucení opětovného překladu vyžaduje nejprve pročistit aktuální objektové a přeložené soubory a následné přeložení pomocí \texttt{make} (viz výše). Pročištění se provede zavoláním utility \texttt{make} s cílem \textit{clean}:
\begin{verbatim}
$ make clean
\end{verbatim}
Výsledné soubory najdete v adresáři \texttt{bin}.

Vzhledem ke skutečnosti, že program je na konkrétním počítači určen spíše k jednorázovému použití (nasbírání potřebných dat), není prováděna jeho instalace a začlenění do operačního systému, tj. není podporován cíl \textit{install} (instalace knihoven, manuálové stránky, atd.).
\subsection{Pomocí QtCreator}
Tato možnost není preferovaná, raději použijte utilitku \texttt{make} s přiloženým \texttt{makefile}. Pokud se přeci jen rozhodnete překládat tímto způsobem (zřejmě z důvodu vývoje), je nutné nejprve otevřít a přeložit projekt \texttt{jsoncpp\_lib}, protože výsledný binární soubor je potřeba při linkování všech ostatních modulů i hlavního programu. Na pořadí překladu ostatních projektů nezáleží. Výsledné soubory jsou ve složce \texttt{debug}.
\subsection{Síťový odpovídač}
Program \texttt{net-echo} (pomocná utilita pro síťové testy v modulu \texttt{net}) se překládá samostatně, do příkazové řádky napište následující (aktuální adresář je \texttt{net-echo}):
\begin{verbatim}
$ g++ -Wall -O3 -std=c++11 net-echo.cpp -o net-echo
\end{verbatim}
Výsledný spustitelný soubor bude umístěn v aktuálním adresáři pod jménem \texttt{net-echo}.

%=====Spuštění=====
\section{Spuštění}
Aplikace se spouští běžným způsobem z příkazového řádku, např. z adresáře \texttt{bin} takto:
\begin{verbatim}
$ ./benchmark
\end{verbatim}
\subsection{Přepínače}
\begin{verbatim}
benchmark [-h] [-a | -i list | -e list] [-v] [-l] [-t number] [-c file]
\end{verbatim}
\begin{tabular}{l p{15cm}}
\texttt{-h}&(\texttt{-{}-help}) vytiskne seznam všech dostupných přepínačů a krátkou nápovědu ke každému z nich\\
\texttt{-a}&(\texttt{-{}-all}) spustí všechny testy, které najde ve stejném adresáři s hlavním programem a jejichž název odpovídá regulárnímu výrazu \texttt{bm\_[\textasciicircum \textbackslash .]+\textbackslash .so}. Tento přepínač je ve výchozím stavu zapnutý.\\
\texttt{-i}&(\texttt{-{}-include}) spustí pouze vyjmenované testy. Argument je povinný, obsahuje seznam jmen modulů oddělených čárkou. Například \texttt{-i mem,net,crypt}\\
\texttt{-e}&(\texttt{-{}-exclude}) spustí všechny testy kromě těch, které jsou vyjmenovány. Argument je povinný, obsahuje seznam modulů oddělených čárkou (stejné jako u \texttt{-i}).\\
\texttt{-v}&(\texttt{-{}-verbose}) vypisuje obsáhlejší informace. Smysluplné použití je při spuštění testů nebo při výpisu modulů přepínačem \texttt{-l}.\\
\texttt{-l}&(\texttt{-{}-list}) vypíše seznam načtených modulů. S přepínačem \texttt{-v} vypíše navíc ke každému modulu seznam obsahujících testů a konfigurační parametry náležejcí k danému modulu. Lze kombinovat i s přepínači \texttt{-i} a \texttt{-e}\\
\texttt{-t}&(\texttt{-{}-times}) určuje, kolikrát se má každý test spustit. Povinný argument obsahuje celé kladné číslo.\\
\texttt{-c}&(\texttt{-{}-config}) nastaví cestu a název konfiguračního souboru. Argument je povinný, může obsahovat relativní i absolutní cestu, případně pouze jméno souboru, pokud je ve stejném adresáři s hlavním programem. Pokud přepínač není uveden, program zkusí najít soubor \texttt{config.json} ve stejném adresáři. V případě jeho neexistence se použijí ve všech případech výchozí hodnoty.
\end{tabular}
\subsection{Výstup}
Veškeré informace program vypisuje na standartní výstup. Je tedy možné využít různých možností shellu jako přesměrování, roury a podobně.

Při spuštění v normálním (zkráceném) módu je na každé řádce vypsáno jen jméno testu a dva časy, vždy oddělené jednou mezerou. První čas je reálný čas doby běhu bez ohledu na plánování vlákna na procesoru a dalších okolnostech. Druhý čas je čas skutečně využitý procesorem (CPU time). Tento čas charakterizuje využití samotného procesoru (tj. není započítána režie operačního systému, čekání na periferie apod.). Oba časy jsou uváděny v~sekundách jako desetinná čísla.

V módu s rozšířeným výpisem je výpis podrobnější, zanamenává jednotlivé kroky, konfigurační parametry, případně vyhodnocuje výsledek měření.
\subsection{Návratové hodnoty}
Pokud program neočekávaně skončí, vypíše na výstup důvod svého ukončení, případně lokalizaci chyby. Pro použití ve skriptech je důležité znát i návratové hodnoty programu:
\begin{tabular}{l p{14cm}}
\textbf{0}&vše proběhlo v pořádku,\\
\textbf{1}&špatně zadaný parametr při spuštění,\\
\textbf{2}&nepovedlo se načíst moduly testů,\\
\textbf{3}&nepovedl se načíst parametry z konfiguračního souboru, pravděpodobně z důvodu syntaktické chyby,\\
\textbf{4}&některý parametr v konfiguračním souboru má chybný typ.
\end{tabular}
\subsection{Příklady}
\paragraph{Příklad 1}
\begin{verbatim}
$ ./benchmark -lv -i crypt,mem -c ../config.json
\end{verbatim}
spustí program \texttt{benchmark} v módu s rozšířeným výpisem, tj. budou vypsány jednotlivé testy modulů a konfigurační parametry. Zahrnuty budou pouze moduly \texttt{crypt} a \texttt{mem}. Bude použit konfigurační soubor \texttt{config.json} v nadřazeném adresáři.

Ukázkový výstup:
\begin{verbatim}
Benchmark utility, Petr Stefan 2014
Using config file ../config.json
  parsing successful
---------------------------------------------------------------------------
Module                                                 File                     
---------------------------------------------------------------------------
  crypt - cryptographic algorithms                       bm_crypt.so              
     # aes - AES (Rijndael) block cipher with 256-bit key
     # sha256 - hashing function from SHA-2 family
     # scrypt - key derivation function with high resource usage
         * Params:
             aes buffer size: 8388608B
             aes seed: 1536
             sha256 text: The quick brown fox jumps over the lazy dog.
             sha256 buf size: 16777216B
             sha256 hash size: 32
             scrypt N: 16384
             scrypt r: 8
             scrypt p: 1
             scrypt key size: 64
             scrypt text: pleaseletmein
             scrypt salt: SodiumChloride
  mem - sequential memory access                         bm_mem.so                
     # read_1b - sequential memory read in 1B blocks
     # read_2b - sequential memory read in 2B blocks
     # read_4b - sequential memory read in 4B blocks
     # read_8b - sequential memory read in 8B blocks
     # write_1b - sequential memory write in 1B blocks
     # write_2b - sequential memory write in 2B blocks
     # write_4b - sequential memory write in 4B blocks
     # write_8b - sequential memory write in 8B blocks
     * Params:
         memory size: 134217728B
         read loop: 1
         write loop: 1
---------------------------------------------------------------------------
\end{verbatim}
\paragraph{Příklad 2}
\begin{verbatim}
$ ./benchmark --exclude net,mem,card --times=2
\end{verbatim}
spustí testy všech modulů kromě modulu \texttt{net}, \texttt{mem} a \texttt{card} s krátkým výpisem. Každý test poběží dvakrát za sebou.

Ukázkový výstup:
\begin{verbatim}
Benchmark utility, Petr Stefan 2014
Config file not found. Using default values.
aes 0.157309870 0.157200137
aes 0.143516245 0.143447975
sha256 0.088802711 0.088753211
sha256 0.089191121 0.089143166
scrypt 0.096577911 0.096504137
scrypt 0.096115013 0.096047694
multiply_4 0.022833923 0.022822420
multiply_4 0.022617914 0.022606185
multiply_8 0.027534580 0.027519842
multiply_8 0.027608007 0.027591479
strassen_4 0.028177454 0.028134703
strassen_4 0.028517904 0.028473959
strassen_8 0.030291520 0.030252664
strassen_8 0.031564896 0.031522611
quicksort 0.082895293 0.082807705
quicksort 0.083616394 0.083547283
cache_seq 0.035291953 0.035240373
cache_seq 0.035866852 0.035813717
cache_rnd 0.187509938 0.187297923
cache_rnd 0.187944289 0.187758450
cache_loc 0.098467130 0.098336589
cache_loc 0.100158890 0.100028170
merge_1 0.034152339 0.034124470
merge_1 0.033661527 0.033633045
merge_2 0.042641756 0.042610674
merge_2 0.042377866 0.042346831
hash_1 0.034419833 0.034332984
hash_1 0.034307898 0.034230757
hash_2 0.036093929 0.036002923
hash_2 0.035845823 0.035762003
levenshtein 0.028084336 0.028025202
levenshtein 0.024360646 0.024292635
zlib 0.162685772 0.162545226
zlib 0.162716924 0.162561484
dijk_n2 1.504898174 1.504352828
dijk_n2 1.500968221 1.500474267
dijk_fast 0.002152368 0.002149613
dijk_fast 0.002195238 0.002192476
\end{verbatim}
\subsection{Program net-echo}
Pomocný program \texttt{net-echo} se spouští podobným způsobem jako hlavní aplikace:
\begin{verbatim}
$ ./net-echo
\end{verbatim}
Podporuje tři přepínače -- \texttt{-h} (\texttt{-{}-help}), \texttt{-t} (\texttt{-{}-tcp}) a \texttt{-u} (\texttt{-{}-udp}). První zmíněný vypíše krátkou nápovědu, ostatní dva přijímají jeden povinný parametr a to číslo příslušného portu, kde program naslouchá. Pokud nejsou uvedeny, výchozí hodnota obou přepínačů je 10 000.
\paragraph{Ukázkový výstup}
\begin{verbatim}
$ ./net-echo
Network echo for benchmark (TCP/UDP, IPv4/IPv6)
  listening on TCP port 10000
  listening on UDP port 10000
Waiting for connection ... (Ctrl-C to quit)
Connection detected ...
  on UDP port from node [::ffff:127.0.0.1]:40643
  262144 bytes echoed
Waiting for connection ... (Ctrl-C to quit)
Connection detected ...
  on TCP port from node [::ffff:127.0.0.1]:49779
  8388608 bytes echoed
Waiting for connection ... (Ctrl-C to quit)
\end{verbatim}

%=====Popis modulů=====
\section{Popis modulů}
\label{sec:popis_modulu}
Následuje popis každého modulu. Vždy je uveden seznam testů v~modulu a stručný popis algoritmů. Ke každému modulu náleží také popis jeho parametrů, které je možno nastavit v~konfiguračním souboru. U každé položky je vždy v závorce uveden typ a výchozí hodnota. Používají se konstanty dvou typů -- UInt a String. UInt je celé kladné číslo (typ \texttt{unsigned int} se všemi jeho omezeními -- např. velikost), String je znakový řetězec (nutno psát v~uvozovkách).
%---Cache---
\subsection{cache}
Modul \texttt{cache} zkouší procesorovou cache a překlad virtuálních adres\footnote{TLB -- \url{http://en.wikipedia.org/wiki/Translation\_lookaside\_buffer}}. Obsahuje tři testy -- sekvenční, náhodný a náhodný s částečnou lokalitou. Všechny testy probíhají tak, že se vytvoří pole struktur. Velikost struktury musí být konstanta známá v době překladu, počet struktur v poli lze konfigurovat. V průběhu testu se z každé struktury přečte jen prvních 32 bitů, ostatní data ve struktuře nás nezajímají. Výsledkem testu je rychlost čtení a doba potřebná k přečtení dat z jedné struktury.
\paragraph{Sekvenční test} prochází struktury v pořadí, v jakém jsou uloženy v připraveném poli.
\paragraph{Náhodný test} prochází struktury v naprosto náhodném pořadí. Jako generátor náhodných indexů je použit \texttt{std::default\_random\_engine} s rovnoměrným rozdělením přes všechny indexy pole struktur.
\paragraph{Náhodný test s částečnou lokalitou} probíhá podobně jako náhodný test, pouze následující index čtené struktury se liší od aktuálního indexu o méně než určenou hodnotu. V tomto omezeném intervalu se použije stejný generátor jako v předchozím testu, rovnoměrné rozdělení je omezenou pouze na aktuální interval. První čtení probíhá v nastaveném intervalu okolo středu pole.
\subsubsection{Parametry}
\begin{tabular}{l p{12cm}}
\textit{cache-struct\_count}&(UInt, 1 000 000) počet struktur v poli\\
\textit{cache-read\_count}&(UInt, 10 000 000) počet čtení při testu. V případě sekvenčního testu se začíná opět od začátku pole.\\
\textit{cache-locality}&(UInt, 3 000) interval pro náhodný test s lokalitou. Index další struktury bude v intervalu [--\textit{cache-locality}, +\textit{cache-locality}] od aktuálního indexu.\\
\textit{cache-seed}&(UInt, 2 563) semínko náhodného generátoru pro náhodný test a náhodný test s částečnou lokalitou
\end{tabular}
%---Card---
\subsection{card}
Tento modul zaštiťuje testy čtení a zápisu na disk. Raspberry Pi nemá klasický HDD (lze připojit přes USB rozhraní), ale používá souborový systém na SD kartě, proto je název tohoto modulu \texttt{card}. Obsahuje tři testy -- sekvenční zápis do souboru, sekvenční čtení souboru a náhodné čtení souboru. Používají se linuxové nízkoúrovňové fukce \texttt{open()}, \texttt{read()}, \texttt{write()}, \texttt{pread()}, \texttt{close()}, nikoli jejich ekvivalenty ze standardní C knihovny čí dokonce C++ proudy. Všechny testy probíhají na náhodných datech po volitelně velkých blocích. Jméno a cesta k pomocnému souboru jdou též konfigurovat, po dokončení testu se soubor vždy odstraní.
\subsubsection{Parametry}
\begin{tabular}{l p{11cm}}
\textit{card-buffer\_size}&(UInt, 51 200) velikost bufferu v bytech, který se používá u~sekvenčních testů\\
\textit{card-repetition}&(UInt, 100) určuje kolikrát se buffer zapíše do souboru za sebou. Velikost souboru v bytech je tedy\\&\centerline{\textit{card-buffer\_size} $*$ \textit{card-repetition}.}\\
\textit{card-rand\_reads}&(UInt, 1 280 000) počet čtení v náhodném testu\\
\textit{card-rnd\_read\_buf\_size}&(UInt, 4) velikost bufferu v bytech při testu s náhodným čtením\\
\textit{card-seed}&(UInt, 2 678) semínko náhodného generátoru pro zápis náhodných dat do souboru a také pro pozici čtení v náhodném testu\\
\textit{card-file}&(String, "./tmp\_card\_tests") cesta a jméno dočasného souboru používaného při testech v tomto modulu. Je třeba respektovat přístupová práva k souborovému systému.
\end{tabular}
\paragraph{Upozornění} Pro zachování správných výsledků testu dejte pozor, aby\\\centerline{\textit{card-buffer\_size} $*$ \textit{card-repetition} $=$ \textit{card-rand\_reads} $*$ \textit{card-rnd\_read\_buf\_size}.}\\To zaručí, že se v obou čtecích testech bude načítat stejná velikost dat.
%---Crypt---
\subsection{crypt}
Modul \texttt{crypt} obsahuje tři známé a rozšířené algoritmy z oblasti kryptografie. AES\footnote{\url{http://cs.wikipedia.org/wiki/Advanced\_Encryption\_Standard}}, SHA256\footnote{\url{http://cs.wikipedia.org/wiki/Secure\_Hash\_Algorithm}, příp. \url{http://en.wikipedia.org/wiki/SHA-2}} a Scrypt\footnote{\url{http://en.wikipedia.org/wiki/Scrypt}}. Ke všem testům jsou použity nějaké referenční či jinak důvěryhodné implementace v jazyce C. Všem autorům děkuji, původní licenční text je vždy součástí hlavního zdrojového souboru každého algoritmu. Test u algoritmu AES provádí šifrování i dešifrování pro kontrolu správné činnosti, u ostatních dvou toto možné z principu není, proto se výsledný hash pro kontrolu vypisuje v módu s rozšířeným výpisem na výstup.
\subsubsection{Parametry}
\begin{tabular}{l p{11cm}}
\textit{crypt-aes\_buf\_size}&(UInt, 8 388 608) velikost bufferu v bytech pro algoritmus AES\\
\textit{crypt-aes\_seed}&(UInt, 1 536) semínko náhodného generátoru pro přípravu náhodných vstupních dat\\
\textit{crypt-sha256\_text}&(String, "The quick brown fox jumps over the lazy dog.") vstupní text pro spočtení SHA-256 hashe\\
\textit{crypt-sha256\_buf\_size}&(UInt, 16 777 216) velikost pracovního bufferu algoritmu SHA\\
\textit{crypt-sha256\_hash\_size}&(UInt, 32) délka výsledného hashe v bytech. Pro zachování algoritmu SHA-256 tuto hodnotu neupravujte (délka 64 bytů odpovídá algoritmu SHA-512).\\
\textit{crypt-scrypt\_N}&(UInt, 16 384) hodnota N pro algoritmus Scrypt\\
\textit{crypt-scrypt\_r}&(UInt, 8) hodnota r pro algoritmus Scrypt\\
\textit{crypt-scrypt\_p}&(UInt, 1) hodnota p pro algoritmus Scrypt\\
\textit{crypt-scrypt\_key\_size}&(UInt, 64) velikost výsledného hashe algoritmu Scrypt v bytech\\
\textit{crypt-scrypt\_text}&(String, "pleaseletmein") vstupní text algoritmu Scrypt\\
\textit{crypt-scrypt\_salt}&(String, "SodiumChloride") sůl algoritmu Scrypt
\end{tabular}
\paragraph{Poznámka} Parametry pro algoritmus Scrypt jsou podrobně vysvětleny v referenčním článku\footnote{\url{http://www.tarsnap.com/scrypt/scrypt.pdf}}.
%---Graph---
\subsection{graph}
V tomto modulu je pouze jeden algoritmus, ale ve dvou implementacích. Jedná se o Dijkstrův algoritmus\footnote{\url{http://cs.wikipedia.org/wiki/Dijkstrův\_algoritmus}} na hledání nejkratší cesty v ohodnoceném orientovaném grafu. Hrany grafu jsou náhodně ohodnoceny celými čísly v intervalu $[1,10]$. Graf je čtvercová síť s diagonálami napříč směru spojnice zdrojového a cílového vrcholu (viz obr. \ref{fig:graph_image}). Všechny hrany jsou orientovány oběma směry, ceny mohou být rozdílné. Obě implementace jsou spouštěny na identických vstupních datech. Rozdíl je ve způsobu vybírání \uv{nejlepšího} vrcholu. To je ten vrchol ve frontě na zpracování, který má nejnižší vzdálenost. První, klasická implementace v čase $O(|V|^2)$ projde celou frontu a vybere z ní odpovídající vrchol. Druhá využívá prioritní frontu ve formě \texttt{std::set}, snižování vzdáleností vrcholů ve frontě se provádí jejich vyjmutím a opětovným vložením s upravenou hodnotou vzdálenosti. Tato upravená verze má časovou složitost $O((|V|+|E|) \log{|V|})$, je vhodná pro \uv{řídké} grafy $(|E|\approx |V|)$.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.45\textwidth]{graph.png}
    \caption{Příklad struktury grafu pro parametr \textit{graph-side} $= 7$. Všechny hrany jsou orientované oběma směry, každý směr má svou cenu. Algoritmus hledá nejkratší cestu ze zeleného do červeného vrcholu.}
    \label{fig:graph_image}
\end{figure}
\subsubsection{Parametry}
\begin{tabular}{l p{13cm}}
\textit{graph-side}&(UInt, 64) počet vrcholů na jedné straně čtvercové mříže, ve které se vytváří vstupní graf. Celkový počet vrcholů je tato hodnota umocněna dvěma.\\
\textit{graph-seed}&(UInt, 13 795) semínko náhodného generátoru pro přiřazování cen hranám grafu
\end{tabular}
%---Join---
\subsection{join}
Tento modul je zodpovědný za databázové algoritmy join. Jedná se o hash join\footnote{\url{http://en.wikipedia.org/wiki/Hash\_join}} a merge join\footnote{\url{http://en.wikipedia.org/wiki/Sort-merge\_join}}, které různými cestami hledají průnik dvou zadaných množin. Vstupní množiny jsou generovány náhodně. Hashování je prováděno prostředky jazyka C++, konkrétně použitím \texttt{std::unordered\_set}. Oba algoritmy jsou implementovány ve dvou verzích -- jen 32-bitové klíče a 32-bitové klíče s 32-bitovými hodnotami.
\subsubsection{Parametry}
\begin{tabular}{l p{12cm}}
\textit{join-set1\_count}&(UInt, 300 000) velikost první vstupní množiny\\
\textit{join-set2\_count}&(UInt, 201 000) velikost druhé vstupní množiny\\
\textit{join-seed}&(UInt, 4 789) semínko náhodného generátoru
\end{tabular}
%---Levenshtein---
\subsection{levenshtein}
V modulu \texttt{levenshtein} je pouze jeden test, algoritmus dynamického programování na počítání Levenshteinovy vzdálenosti dvou řetězců\footnote{\url{http://en.wikipedia.org/wiki/Levenshtein\_distance}}. Byl implementován iterativní přístup s plnou maticí. Oba vstupní řetězce jsou konfigurovatelné. Výsledná vzdálenost je vypsána při spuštění ve verbose módu.
\subsubsection{Parametry}
\begin{tabular}{l p{12cm}}
\textit{levenshtein-first}&(String, ...) první vstupní řetězec\\
\textit{levenshtein-second}&(String, ...) druhý vstupní řetězec
\end{tabular}
\paragraph{Poznámka} Oba vstupní řetězce jsou dosti dlouhé, proto zde nejsou uvedeny výchozí hodnoty. Zájemci mohou nahlédnout do ukázkového konfiguračního souboru.
%---Matrix---
\subsection{matrix}
Modul \texttt{matrix} obsahuje testy na násobení čísel s plovoucí desetinnou čárkou, konkrétně typů \texttt{float} a \texttt{double}. Obsahuje čtyři testy -- klasické násobení matic v obou typech a násobení matic podle Strassenova vzorce\footnote{\url{http://cs.wikipedia.org/wiki/Strassenův\_algoritmus}} v obou typech. Všechny matice jsou čtvercové s konfigurovatelným rozměrem. Hodnoty prvků v maticích jsou generovány náhodně s normálním rozdělením\footnote{$\mathcal{N}\left(\mu,\sigma ^2\right)$ -- \url{http://en.wikipedia.org/wiki/Normal\_distribution\#Notation}} $\mathcal{N}\left(\frac{1}{\sqrt{\operatorname{\textit{matrix-size}}}}, \left(5 \cdot \frac{1}{\sqrt{\operatorname{\textit{matrix-size}}}}\right)^2\right)$, což zajišťuje, že hodnoty ve výsledné matici budou dostatečně malé (okolo 1) a nebude docházet k přetečení datových typů v průběhu výpočtu. Vstupní matice pro testy prováděné se stejným datovým typem jsou identické. 
\subsubsection{Parametry}
\begin{tabular}{l p{12cm}}
\textit{matrix-size}&(UInt, 256) velikost vstupních i výstupních matic (matice jsou čtvercové, tedy ve výchozím stavu $256 * 256$)\\
\textit{matrix-seed}&(UInt, 6 248) semínko náhodného generátoru
\end{tabular}
%---Mem---
\subsection{mem}
Tento modul se zabývá měřením rychlosti čtení a zápisu do paměti RAM. Testy probíhají v nastavitelných paměťových blocích. Čtení i zápis jsou prováděný ve verzích s velikostmi bloků 1 byte (\texttt{uint8\_t}), 2 byty (\texttt{uint16\_t}), 4 byty (\texttt{uint32\_t}) a 8 bytů (\texttt{uint64\_t}). Hodnoty, které se zapisují a při čtení jsou kontrolovány jsou vytořeny z bytů \texttt{0x55} (\texttt{0x55}, \texttt{0x5555}, \texttt{0x55555555}, \dots).
\subsubsection{Parametry}
\begin{tabular}{l p{12cm}}
\textit{mem-memory\_size}&(UInt, 134 217 728) velikost využité paměti v každém testu v bytech. Pro správné výsledky se doporučuje volit tuto velikost jako násobek největšího bloku, tj. 8 bytů.\\
\textit{mem-read\_loop}&(UInt, 1) počet opakovaného čtení celého paměťového bloku ve všech čtecích testech\\
\textit{mem-write\_loop}&(UInt, 1) počet opakovaného zápisu celého paměťového bloku ve všech zapisovacích testech
\end{tabular}
%---Net---
\subsection{net}
V modulu \texttt{net} je celkem sedm testů. První je na měření latence, zbývající měří propustnost ethernetového adaptéru. Všechny testy probíhají na náhodných datech. Latence se měří pouze na UDP s malými bloky (které se vejdou do jednoho paketu), výsledek je průměrná doba odpovědi na paket. Ostatní testy probíhají na náhodných datech po různě velkých blocích (2 kB, 8 kB a 32 kB), vždy přes TCP i UDP. Celková velikost posílaných dat je jeden z konfigurovatelných parametrů. Výsledkem těchto testů je rychlost přenosu dat v~Mbit/s. Všechny testy potřebují ke svému běhu spuštěný pomocný program \texttt{net-echo}, který je dodáván společně s ostatními zdrojovými soubory, jen je nutné ho zkompilovat zvlášť (viz sekce \ref{sec:sestaveni}). Samotné testy probíhají tak, že se odešle buffer nastavené velikosti přes síťový adaptér na pomocný počítač s běžícím odpovídačem \texttt{net-echo}. Ten obratem data pošle zpět a testovaný počítač data přijme. Provádí se kontrola, zda přijatá data odpovídají odeslaným. To se opakuje do vyčerpání vstupních dat.

Předpokládá se, že počítač s odpovídačem je výrazně rychlejší než testovaný stroj a je připojen v místní síti. Za těchto podmínek není výsledek měření latence ani propustnosti významněji ovlivněn.
\subsubsection{Parametry}
\begin{tabular}{l p{12cm}}
\textit{net-test\_size}&(UInt, 8 388 608) celková velikost odesílaných dat v testech na propustnost\\
\textit{net-ping\_count}&(UInt, 4 096) počet odeslaných paketů při měření latence\\
\textit{net-ping\_data}&(UInt, 64) velikost bloku dat posílaných při měření latence v bytech. Tato velikost by měla být dostatečně malá, aby nedocházelo k rozdělení dat do více paketů.\\
\textit{net-server}&(String, "localhost") doménové jméno či IPv4 nebo IPv6 adresa vzdáleného počítače, proti kterému je prováděn test\\
\textit{net-tcp\_port}&(UInt, 10 000) TCP port na vzdáleném počítači\\
\textit{net-udp\_port}&(UInt, 10 000) UDP port na vzdáleném počítači\\
\textit{net-seed}&(UInt, 224 120) semínko náhodného generátoru
\end{tabular}
\paragraph{Poznámka} Raspbian má v továrním nastavení podporu IPv6 z důvodu šetření pamětí vypnutou, lze ji však snadnou zapnout.
%---Sort---
\subsection{sort}
Modul \texttt{sort} obsahuje jediný test, třídění pole prvků typu \texttt{int} pomocí knihovní implementace algoritmu QuickSort\footnote{\url{http://cs.wikipedia.org/wiki/Quicksort}} (\texttt{std::sort}). Vstupní data jsou generována náhodně.
\subsubsection{Parametry}
\begin{tabular}{l p{12cm}}
\textit{sort-items}&(UInt, 1 153 096) počet prvků ke třídění\\
\textit{sort-seed}&(UInt, 7 615) semínko náhodného generátoru
\end{tabular}
%---Zlib---
\subsection{zlib}
V modulu \texttt{zlib} je klasický zástupce komprimačních algoritmů, knihovna zlib\footnote{\url{http://en.wikipedia.org/wiki/Zlib}}. Zdrojové soubory jsou převzaty z referenční implementace a jsou napsány v jazyce C. Původní text licence je součástí zdrojových souborů. Vstupní data se vytvoří opakováním vstupní znakové sekvence do zaplnění velikosti bufferu a náhodným přepisem některých písmen. Test měří čas komprese i dekomprese zároveň, kontroluje se shoda výsledku s originálními daty.
\subsubsection{Parametry}
\begin{tabular}{l p{12cm}}
\textit{zlib-buf\_size}&(UInt, 8 388 608) velikost vstupního bufferu pro knihovny v bytech\\
\textit{zlib-text}&(String, "The quick brown fox jumps over the lazy dog.") vstupní text jako základ vstupu pro zpracování algoritmem\\
\textit{zlib-seed}&(UInt, 1 536) semínko náhodného generátoru
\end{tabular}
\end{document}

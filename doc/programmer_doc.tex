\documentclass[12pt,a4paper,titlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage[T1]{fontenc}
\usepackage{a4wide}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage[hypcap]{caption}
\usepackage{color}
\usepackage{listings}
\usepackage[affil-it]{authblk}
\author{Petr Stefan}
\affil{Matematicko-fyzikální fakulta Univerzity Karlovy v Praze}
\date{\today}
\title{\textbf{Program benchmark}\\\ \\Programátorská dokumentace}
\lstset{
    breaklines=true,
    xleftmargin=\parindent,
    language=C++,
    showstringspaces=false,
    basicstyle=\footnotesize\ttfamily,
    keywordstyle=\color[rgb]{0,0,1},
    commentstyle=\color[rgb]{0.026,0.112,0.095},
%    identifierstyle=\color{blue},
    stringstyle=\color[rgb]{0.627,0.126,0.941},
}
\begin{document}

\maketitle

%=====Úvod=====
\section{Úvod}
Programátorská dokumentace by měla pomoci v orientaci ve stávajícím kódu a usnadnit vývoj nových modulů. Následující text předpokládá znalost uživatelské dokumentace k~programu \texttt{benchmark} a alespoň základní znalost Linuxu a programování v jazyce C++.

\section{Rozhraní modulů}
Modul obsahuje jeden nebo více testů (každý ve třídě \texttt{test}), jejichž instance jsou sdruženy do třídy reprezentující skupinu testů (\texttt{test\_suite}). Instanci této třídy vytvoří tovární funkce modulu.

Každý modul musí implementovat všechny třídy z hlavičkového souboru \texttt{test.h}. Dále musí obsahovat tovární funkci \texttt{create()} typu deklarovaného tamtéž. Následuje popis jednotlivých částí.
\subsection{Třída test}
\begin{lstlisting}
class test {
public:
    virtual std::string get_info() = 0;
    virtual bool prepare_environment() = 0;
    virtual void run_test() = 0;
    virtual bool check_result() = 0;
    virtual void evaluate(double time) = 0;
    virtual std::string get_last_error() = 0;
    virtual bool clear_environment() = 0;
    virtual ~test() {}
};
\end{lstlisting}
Každý test musí dědit od této třídy a implementovat všechny její metody. Test musí být opakovatelný, tj. všechna vstupní data si musí sám připravit a následně je po sobě uklidit. Průběh testování a pořadí volání jednotlivých metod z hlavního programu naleznete v~sekci~\ref{sec:hlavni_aplikace}.\\

\paragraph{get\_info()} Tato metoda vrací krátký popisek testu, např. \uv{quicksort - sorting vector of items}. Měl by zachovávat tento formát: název testu, mezera, pomlčka, mezera a krátký popisek o testu.
\paragraph{prepare\_environment()} Tato metoda má za úkol připravit data pro testování. Může se jednat o alokaci paměti, nastavení náhodného generátoru čísel, vyplnění pole těmito čísly, atd. Pokud je nastaven verbose přepínač na \texttt{true}, pak na výstup vypíše konfigurovatelné parametry s jejich aktuálními hodnotami, případně nějaké další doplňující informace. Návratová hodnota indikuje, zda vše proběhlo úspěšně a je možno spustit testování.
\paragraph{run\_test()} V těle této metody je implementován vlastní algoritmus, který chceme testovat. Z důvodu měření času běhu této metody se kontrola výsledků provádí až dále.
\paragraph{check\_result()} Tato metoda má možnost zkontrolovat správnost výsledku. Zda je výsledek korektní dává vědět prostřednictvím návratové hodnoty. Některé testy z principu nemají žádná data ke kontrole či se výsledek zkontrolovat nedá, pak tato metoda bude vždy vracet \texttt{true}.
\paragraph{evaluate(double)} Tato metoda umožňuje vypsat výsledek testu. Je možné použít vstupní parametr, který reprezentuje reálnou dobu běhu testu ve vteřinách, např. pro výpočet rychlosti zápisu dat na disk.
\paragraph{get\_last\_error()} Pokud nějaká metoda neuspěje, měla by nastavit chybovou zprávu, která daný problém popíše. Tato metoda vrátí poslední takto nastavenou zprávu.
\paragraph{clear\_environment()} Tato metoda uklidí po testu. To může znamenat dealokace paměti, uzavření souboru či socketu a další.
\paragraph{\textasciitilde test()} Virtuální destruktor. Nutný z důvodu odkazování se na jednotlivé implementace testů prostřednictvím tohoto společného předka.
\subsection{Třída test\_suite}
\begin{lstlisting}
class test_suite {
public:
    std::vector<std::unique_ptr<test>> tests;
    void release()
    {
        delete this;
    }
    virtual std::string get_name() = 0;
    virtual void print_params() = 0;
    virtual ~test_suite() {}
};
\end{lstlisting}
Potomek této třídy smí existovat jen jeden v celém modulu. Stará se o kolekci všech testů v aktuálním modulu. Vytvoření konkrétní instance se provede v tovární funkci.\\

\paragraph{tests} Seznam všech testů v aktuálním modulu.
\paragraph{release()} Metoda k destrukci této třídy. Volána je z hlavního programu před ukončením modulu.
\paragraph{get\_name()} Tato metoda vrátí jméno a krátký popis celého modulu, např. \uv{sort - sorting data}. Opět se doporučuje zachovat tento formát popisku.
\paragraph{print\_params()} Tato metoda vypíše konfigurační parametry celého modulu s jejich hodnotami. Volána je při rozšířeném výpisu testů.
\paragraph{\textasciitilde test\_suite()} Virtuální destruktor.
\subsection{Tovární funkce}
\begin{lstlisting}
typedef test_suite* create_t(bool, Json::Value&);
\end{lstlisting}
Tovární funkce v každém modulu musí dodržet tento typ a jméno \uv{create}. Typická deklarace této funkce v každém modulu je:
\begin{lstlisting}
extern "C" test_suite* create(bool verbose_flag, Json::Value& root);
\end{lstlisting}
Ukázkovou implementaci najdete v sekci~\ref{sec:ukazkovy_modul}.\\

\section{Hlavní aplikace}
\label{sec:hlavni_aplikace}
Hlavní aplikace se stará o načítání jednotlivých modulů, spouštění testů, měření času a interakci s uživatelem pomocí parametrů příkazové řádky. Některé doplňující informace jsou uvedeny níže.
\subsection{Přepínače}
Užití přepínačů je podrobně vysvětleno v uživatelské dokumentaci. Zpracování se provádí pomocí knihovní funkce \texttt{getopt\_long()}\footnote{\url{http://linux.die.net/man/3/getopt\_long}} deklarované v hlavičkovém souboru \texttt{getopt.h}. To zaručuje rozpoznávání jak krátkých (\texttt{-h}), tak i dlouhých verzí přepínačů (\texttt{-{}-help}) a také slučování krátkých verzí -- místo \texttt{-l -v} lze psát \texttt{-lv}.
\subsection{Načítání knihoven}
Načítání knihoven probíhá tak, že se nejprve najde složka, ve které je binární soubor hlavního programu. Toto získáme z nultého parametru spuštění (\texttt{argv[0]}). V této složce prohledáváme všechny soubory. Ty, jejichž názvy vyhovují regulárnímu výrazu \texttt{bm\_[\textasciicircum \textbackslash .]+\textbackslash .so}, se v případě uvedení některého z přepínačů \texttt{-i} nebo \texttt{-e} kontrolují se seznamy vytvořenými z povinných argumentů těchto přepínačů a jen vyhovující jména modulů jsou předána dál k samotnému načtení. Pokud žádný z těchto přepínačů není uveden, k načtení jsou předána všechna jména správného formátu. Pro práci se souborovým systémem jsou zde použity Linuxové funkce \texttt{opendir(), readdir(), closedir()}.

Pro vlastní načítání dynamických knihoven jsou použity funkce \texttt{dlopen()}, \texttt{dlsym()} a \texttt{dlclose()}, jak je vidět z ukázky kódu.
\begin{lstlisting}
for(const auto& library : libraries) {
    void* handle = dlopen(library.c_str(), RTLD_NOW);
    if (handle == nullptr)
        std::cout << "Cannot open library '" << library << "': " << dlerror() << std::endl;
    else {
        modules.push_back(handle);
        create_t* create = (create_t*)dlsym(handle, "create");
        if (create == nullptr)
            std::cout << "Failed to link 'create' function in " << library << ": " << dlerror() << std::endl;
        else {
            auto suite = create(verbose_flag != 0, root);
            if(suite != nullptr)
                test_suites.push_back(suite);
            else {
                retval = return_value::BAD_CONF;
                break;
            }
        }
    }
}
\end{lstlisting}
\dots
\begin{lstlisting}
std::for_each(test_suites.begin(), test_suites.end(), [](test_suite* t){t->release();});
std::for_each(modules.begin(), modules.end(), [](void* module){dlclose(module);});
\end{lstlisting}
\subsection{Vlastní testování}
Vlastní testování provádí funkce \texttt{run\_tests()}. Pro každý test se provádí následující kroky.
\begin{enumerate}
\item Zavolání metody \texttt{get\_info()}. V módu s rozšířeným výpisem se vypíše celý vrácený řetězec, jinak pouze jméno testu (uvedeno před pomlčkou).
\item Zavolání metody \texttt{prepare\_environment()}. Chyba vždy vynutí ukončení testu. Zpráva o chybě (získaná pomocí \texttt{get\_last\_error()}) je vždy vypsána na standartní výstup.
\item Začátek měření času.
\item Spuštění testu zavoláním metody \texttt{run\_test()}.
\item Konec měření času.
\item Vypsání času na výstup. Formát opět závisí na aktuálním nastavení přepínače verbose.
\item Kontrola správnosti výsledků zavoláním metody \texttt{check\_result()}. I v případě záporné odpovědi se pokračuje dále. Případná chybová zpráva (opět \texttt{get\_last\_error()}) je vypsána na výstup i v případě režimu zkráceného výpisu.
\item Zavolání metody \texttt{evaluated(double time)}. Tento krok se provede jen v případě zapnutého verbose výpisu a zároveň pokud kontrola výsledků proběhla s výsledkem \texttt{true}. Tato metoda může vypsat dodatečné informace, které mohou záviset na naměřeném čase.
\item Zavolání metody \texttt{clear\_environment()}. Ta zaručí případné uklizení po proběhlém testu a navrácení prostředí do původního stavu.
\end{enumerate}
\subsection{Měření času}
Měření času se provácí pomocí funkce \texttt{clock\_gettime()}. Reálný čas měříme s parametrem \texttt{CLOCK\_MONOTONIC}, pro měření procesorového času si musíme nejprve zjistit ID procesorových hodin, které následně použijeme jako parametr pro \texttt{clock\_gettime()}. Počáteční i koncový čas se ukládají do struktury \texttt{timespec}, výpočet rozdílu těchto časů obstará funkce \texttt{tdiff()}. Ukázka kódu pro zjištění reálného a procesorového času:
\begin{lstlisting}
timespec cpu_begin;
timespec real_begin;
clockid_t clockid;

clock_getcpuclockid(0, &clockid);
clock_gettime(CLOCK_MONOTONIC, &real_begin);
clock_gettime(clockid, &cpu_begin);
\end{lstlisting}

Tento způsob měření času je pro tento účel nejvhodnější. Existují i Linuxové prostředky, které vrací například čas aplikace strávený v systémovém a uživatelském prostoru, ale tyto funkce nevykazují na Raspberry Pi dostatečnou přesnost.

\section{Parser jsoncpp}
Nejlepší zdroj informací o parseru \texttt{jsoncpp} je oficiální dokumentace\footnote{\url{http://open-source-parsers.github.io/jsoncpp-docs/doxygen/index.html}}. O načtení konfiguračního souboru a jeho rozbor (parsování) se stará hlavní program. Protože program \texttt{jsoncpp} nepodporuje ověření pomocí JSON schématu\footnote{\url{http://json-schema.org/latest/json-schema-validation.html}}, správný typ svých parametrů si musí hlídat každý modul zvlášť (ošetření výjimky \texttt{std::runtime\_error}). Bývá zvykem, že v každém modulu je třída \texttt{config}, která načte potřebné parametry a uloží je do svých veřejných proměnných. Instance této třídy je globální v rámci modulu. Viz příklad v~sekci~\ref{sec:ukazkovy_modul}.

\section{Překlad}
Překlad se provádí pomocí přiloženého \texttt{makefile}. Popis použití je součástí uživatelské dokumentace.
\subsection{Struktura makefile}
Na začátku souboru je konfigurační část, ve které jsou nastaveny hlavní proměnné používané při překladu. Jedná se hlavně o překladač, cílový adresář, parametry překladu a linkování a další.

Dále následují parametry pro hlavní aplikaci. Jedná se o seznam zdrojových \texttt{.cpp} a \texttt{.h} souborů a knihoven potřebných pro hlavní program.

Následuje sekce parametrů pro moduly. Důležitá je proměnná \texttt{MODULES}, která obsahuje seznam názvů všech modulů, které se budou překládat. Ke každému modulu je dále třeba vytvořit proměnné, které uvádějí zdrojové soubory, přepínače překladače a další náležitosti. Vždy musí mít prefix jméno modulu, např. \texttt{sort\_} pro modul \texttt{sort}. Existují tyto proměnné -- \texttt{SRC} pro zdrojové \texttt{.cpp} soubory, \texttt{H} pro hlavičkové soubory, \texttt{CPPFLAGS} a \texttt{CFLAGS} přepínače pro překlad C++ a C souborů, \texttt{LFLAGS} přepínače pro linkování a \texttt{LIBS} pro případné dodatečné knihovny.

Poslední logickou sekcí \texttt{makefile} jsou vlastní pravidla pro překlad. Nejdříve jsou uvedena pravidla pro překlad a linkování hlavní aplikace, dále pro sdílenou knihovnu parseru \texttt{libjsoncpp.so} a následuje šablona pro překlad jednotlivých modulů. Instance šablony pro jednotlivé moduly se vytváří hned pod šablonou. Pozor, cíle se překládají v pořadí uvedeném u hlavního cíle \textit{all}, ne v pořadí v jakém jsou za sebou v souboru \texttt{makefile}.

Na konci souboru je ještě cíl \textit{clean}, který odstraní přeložené binární soubory a také objektové soubory, které zbyly z překladu.

\section{Ukázkový modul}
\label{sec:ukazkovy_modul}
Jako ukázkový modul je zde uveden \texttt{sort}. Jeho kód je nejkratší a neimplementuje žádný složitý algoritmus, ale na druhou stranu obsahuje většinu náležitostí včetně kontroly výsledků.\looseness=-1
\begin{lstlisting}
#include <string>
#include <vector>
#include <algorithm>
#include <random>
#include <iostream>

#include "../include/test.h"
#include "../include/json.h"


class config {
public:
    config() : items(0), seed(0) {}
    bool init(Json::Value& root, std::string& error_message)
    {
        try {
            items = root.get("sort-items", 1153096).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'sort-items' parameter read failed.
                Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            seed = root.get("sort-seed", 7615).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'sort-seed' parameter read failed.
                Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        return true;
    }
    size_t items;
    size_t seed;
};

namespace {
    bool verbose;
    config params;
}


template <typename T>
class stdsort : public test {
public:
    stdsort() {}
    virtual std::string get_info()
    {
        return std::string("quicksort - sorting vector of items");
    }
    virtual bool prepare_environment()
    {
        std::minstd_rand0 generator(params.seed);
        data_.reserve(params.items);
        for(size_t i = 0; i < params.items; i++)
            data_.push_back(static_cast<T>(generator()));
        if(verbose) {
            std::cout << "  items: " << params.items << std::endl;
            std::cout << "  item lenght: " << sizeof(T) << std::endl;
            std::cout << "  seed: " << params.seed << std::endl;
        }
        return true;
    }
    virtual void run_test()
    {
        std::sort(data_.begin(), data_.end());
    }
    virtual bool check_result()
    {
        T last = data_[0];
        T current;
        for(size_t i = 1; i < params.items; i++) {
            current = data_[i];
            if(last > current) {
                last_error_ = "Sorting test failed.";
                return false;
            }
            last = current;
        }
        return true;
    }
    virtual void evaluate(double) {}
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        data_.clear();
        return true;
    }
    virtual ~stdsort() {}
private:
    std::string last_error_;
    std::vector<T> data_;
};

class sort_tests : public test_suite {
public:
    sort_tests(void)
    {
        tests.push_back(std::unique_ptr<test>(new stdsort<int>));
    }
    virtual std::string get_name() {
        return std::string("sort - sorting data");
    }
    virtual void print_params()
    {
        std::cout << "   * Params:" << std::endl <<
            "       items: " << params.items << std::endl <<
            "       seed: " << params.seed << std::endl;
    }
    virtual ~sort_tests() {}
};

extern "C" test_suite* create(bool verbose_flag, Json::Value& root)
{
    verbose = verbose_flag;
    std::string error_message;
    if(!params.init(root, error_message)) {
        std::cout << "Config - " << error_message << std::endl;
        return nullptr;
    }
    return new sort_tests();
}
\end{lstlisting}

\end{document}

TEMPLATE = lib
TARGET = bm_crypt
CONFIG += plugin no_plugin_name_prefix

DESTDIR = ../debug

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11
QMAKE_CXXFLAGS += -fPIC
QMAKE_LFLAGS += -shared
QMAKE_CXXFLAGS += -std=c++11
QMAKE_LFLAGS += -Wl,-rpath=\'\$\$ORIGIN\'

LIBS += -L../debug/ -ljsoncpp

SOURCES += main.cpp \
    aes.c \
    scrypt.c \
    sha256.c

HEADERS += \
    ../include/test.h \
    ../include/json.h \
    aes.h \
    scrypt.h \
    sha256.h \
    sysendian.h



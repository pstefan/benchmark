#include <string>
#include <vector>
#include <memory>
#include <random>
#include <iostream>
#include <iomanip>
#include <cstdint>

#include "crypt.h"
#include "sha256.h"
#include "scrypt.h"
#include "aes.h"
#include "../include/test.h"
#include "../include/json.h"

class config {
public:
    config() : aes_buf_size(0), sha256_buf_size(0), sha256_hash_size(0), scrypt_key_size(0) {}
    bool init(Json::Value& root, std::string& error_message)
    {
        try {
            aes_buf_size = root.get("aes_buf_size", 8 * 1024 * 1024).asUInt(); //v Bytech
        }
        catch(std::runtime_error& e) {
            error_message = "'aes_buf_size' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            aes_seed = root.get("aes_seed", 42).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'aes_seed' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            sha256_text = root.get("sha256_text", "The quick brown fox jumps over the lazy dog.").asString();
        }
        catch(std::runtime_error& e) {
            error_message = "'sha256_text' parameter read failed. String expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            sha256_buf_size = root.get("sha256_buf_size", 16 * 1024 * 1024).asUInt(); //v Bytech
        }
        catch(std::runtime_error& e) {
            error_message = "'sha256_buf_size' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            sha256_hash_size = root.get("sha256_hash_size", 32).asUInt(); //v Bytech
        }
        catch(std::runtime_error& e) {
            error_message = "'sha256_hash_size' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            scrypt_N = root.get("scrypt_N", 16384).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'scrypt_N' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            scrypt_r = root.get("scrypt_r", 8).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'scrypt_r' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            scrypt_p = root.get("scrypt_p", 1).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'scrypt_p' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            scrypt_key_size = root.get("scrypt_key_size", 64).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'scrypt_key_size' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            scrypt_text = root.get("scrypt_text", "pleaseletmein").asString();
        }
        catch(std::runtime_error& e) {
            error_message = "'scrypt_text' parameter read failed. String expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            scrypt_salt = root.get("scrypt_salt", "SodiumChloride").asString();
        }
        catch(std::runtime_error& e) {
            error_message = "'scrypt_salt' parameter read failed. String expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        return true;
    }
    size_t aes_buf_size;
    size_t aes_seed;
    std::string sha256_text;
    size_t sha256_buf_size;
    size_t sha256_hash_size;
    size_t scrypt_N;
    size_t scrypt_r;
    size_t scrypt_p;
    size_t scrypt_key_size;
    std::string scrypt_text;
    std::string scrypt_salt;
};


namespace {
    bool verbose;
    config params;
}

//-----aes-----

class aes : public test {
public:
    aes();
    virtual std::string get_name();
    virtual std::string get_info();
    virtual bool prepare_environment();
    virtual void run_test();
    virtual bool check_result();
    virtual void evaluate(double) {}
    virtual std::string get_last_error();
    virtual bool clear_environment();
    virtual ~aes();
private:
    std::string last_error_;
    bool result_;
    uint8_t *buf_src_;
    uint8_t *buf_crypt_;
};

aes::aes() : result_(false), buf_src_(nullptr), buf_crypt_(nullptr)
{
}

std::string aes::get_name()
{
    return std::string("aes");
}
std::string aes::get_info()
{
    return std::string("AES (Rijndael) block cipher with 256-bit key");
}

bool aes::prepare_environment()
{
    // vytvoreni buferu a naplneni obsahem
    std::minstd_rand0 generator(params.aes_seed);
    buf_src_ = new uint8_t [params.aes_buf_size];
    for(size_t i = 0; i < params.aes_buf_size; i++)
        buf_src_[i] = static_cast<uint8_t>(generator());
    buf_crypt_ = new uint8_t [params.aes_buf_size];
    memcpy(buf_crypt_, buf_src_, params.aes_buf_size);
    if(verbose) {
        std::cout << "  data size (aes buf size): " << params.aes_buf_size << "B" << std::endl;
        std::cout << "  seed: " << params.aes_seed << std::endl;
    }
    return true;
}

void aes::run_test()
{
    static const uint8_t key[32] = {
        0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
        0xfe, 0xdc, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10, 0xfe, 0xdc, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10,
    };
    uint8_t iv[16];
    aes_context ctx;

    // sifrovani bufferu (na miste)
    aes_setkey_enc(&ctx, key, 256);
    memset(iv, 0x55, sizeof(iv));
    aes_crypt_cbc(&ctx, AES_ENCRYPT, params.aes_buf_size, iv, buf_crypt_, buf_crypt_);
    // desifrovani na miste
    aes_setkey_dec(&ctx, key, 256);
    memset(iv, 0x55, sizeof(iv));
    aes_crypt_cbc(&ctx, AES_DECRYPT, params.aes_buf_size, iv, buf_crypt_, buf_crypt_);
}

bool aes::check_result()
{
    // kontrola (sifrovani + desifrovani obsah nezmeni)
    if(memcmp(buf_src_, buf_crypt_, params.aes_buf_size) == 0)
        return true;
    else {
        last_error_ = "Encryption/decryption error.";
        return false;
    }
}

std::string aes::get_last_error()
{
    return last_error_;
}

bool aes::clear_environment()
{
    delete [] buf_src_;
    delete [] buf_crypt_;
    buf_src_ = nullptr;
    buf_crypt_ = nullptr;
    return true;
}

aes::~aes()
{
    delete [] buf_src_;
    delete [] buf_crypt_;
}

//-----sha256-----
class sha256 : public test {
public:
    sha256();
    virtual std::string get_name();
    virtual std::string get_info();
    virtual bool prepare_environment();
    virtual void run_test();
    virtual bool check_result();
    virtual void evaluate(double);
    virtual std::string get_last_error();
    virtual bool clear_environment();
    virtual ~sha256();
private:
    std::string last_error_;
    //static const size_t hash_size_ = 32;
    uint8_t *buffer_;
    uint8_t *digest_;
};

sha256::sha256() : buffer_(nullptr), digest_(nullptr)
{
}

std::string sha256::get_name()
{
    return std::string("sha256");
}
std::string sha256::get_info()
{
    return std::string("hashing function from SHA-2 family");
}

bool sha256::prepare_environment()
{
    //const char text[] = "The quick brown fox jumps over the lazy dog.";
    buffer_ = new uint8_t[params.sha256_buf_size];
    digest_ = new uint8_t[params.sha256_hash_size];
    // bufer naplnim opakovanim textu
    size_t used = 0;
    size_t text_size = params.sha256_text.size() + 1;
    while(used + text_size < params.sha256_buf_size) {
        memcpy(buffer_ + used, params.sha256_text.c_str(), text_size);
        used += text_size;
    }
    memcpy(buffer_ + used, params.sha256_text.c_str(), params.sha256_buf_size - used);
    if(verbose) {
        std::cout << "  text (sha256 text): " << params.sha256_text << std::endl;
        std::cout << "  data size (sha256 buf size): " << params.sha256_buf_size << "B" << std::endl;
        std::cout << "  hash size (sha256 hash size): " << params.sha256_hash_size << "B" << std::endl;
    }
    return true;
}

void sha256::run_test()
{
    SHA256_Buf(buffer_, params.sha256_buf_size, digest_);
}

bool sha256::check_result()
{
    /* Kontrola pro hash_size 32 a text "The quick brown fox jumps over the lazy dog."

    static const uint8_t sha_hash[hash_size_] = {
        0x97, 0x87, 0x7c, 0x5f, 0xb4, 0xa3, 0x05, 0x08, 0x13, 0x51, 0x6d, 0x26, 0x53, 0xb3, 0xcb, 0xe0,
        0xb5, 0xb7, 0x43, 0xbc, 0x3a, 0x6f, 0x67, 0x57, 0x4c, 0x10, 0x21, 0x68, 0x8b, 0xb0, 0x2b, 0x60
    };

    // kontrola na predvypoctenou hodnotu
    if(memcmp(digest_, sha_hash, hash_size) != 0) {
        last_error_ = "Resulted value different.";
        return false;
    }
    else
        return true;
    */
    return true;
}

void sha256::evaluate(double)
{
    std::cout << "Evaluated: " << std::endl << "  sha256 hash: " << std::showbase << std::internal << std::setfill('0');
    for(size_t i = 0; i < params.sha256_hash_size; i++) {
        if(i % 8 == 0) {
            std::cout << std::endl << "    ";
        }
        std::cout << std::hex << std::setw(4) << static_cast<int>(digest_[i]) << std::dec << " ";
    }
    std::cout << std::dec << std::endl;
}

std::string sha256::get_last_error()
{
    return last_error_;
}

bool sha256::clear_environment()
{
    delete[] buffer_;
    buffer_ = nullptr;
    delete[] digest_;
    digest_ = nullptr;
    return true;
}

sha256::~sha256()
{
    delete[] buffer_;
    delete[] digest_;
}


//-----scrypt----
class scrypt : public test {
public:
    scrypt();
    virtual std::string get_name();
    virtual std::string get_info();
    virtual bool prepare_environment();
    virtual void run_test();
    virtual bool check_result();
    virtual void evaluate(double);
    virtual std::string get_last_error();
    virtual bool clear_environment();
    virtual ~scrypt();
private:
    std::string last_error_;
    bool result_;
    //static const size_t key_size_ = 64;
    uint8_t *key_;
};

scrypt::scrypt() : result_(false), key_(nullptr)
{
}

std::string scrypt::get_name()
{
    return std::string("scrypt");
}
std::string scrypt::get_info()
{
    return std::string("key derivation function with high resource usage");
}

bool scrypt::prepare_environment()
{
    key_ = new uint8_t[params.scrypt_key_size];
    if(verbose) {
        std::cout << "  N: " << params.scrypt_N << std::endl << "  r: " << params.scrypt_r << std::endl <<
                     "  p: " << params.scrypt_p << std::endl << "  key size: " << params.scrypt_key_size <<
                     std::endl << "  text: " << params.scrypt_text << std::endl << "  salt: " <<
                     params.scrypt_salt << std::endl;
    }
    return true;
}

void scrypt::run_test()
{
    /*static const char text[] = "pleaseletmein";
    static const char salt[] = "SodiumChloride";*/

    result_ = crypto_scrypt((const uint8_t *)(params.scrypt_text.c_str()), strlen(params.scrypt_text.c_str()),
                            (const uint8_t *)(params.scrypt_salt.c_str()), strlen(params.scrypt_salt.c_str()),
                            params.scrypt_N, params.scrypt_r, params.scrypt_p, key_, params.scrypt_key_size) == 0;
}

bool scrypt::check_result()
{
    /*static const uint8_t scrypt_hash[key_size_] = {
        0x70, 0x23, 0xbd, 0xcb, 0x3a, 0xfd, 0x73, 0x48, 0x46, 0x1c, 0x06, 0xcd, 0x81, 0xfd, 0x38, 0xeb,
        0xfd, 0xa8, 0xfb, 0xba, 0x90, 0x4f, 0x8e, 0x3e, 0xa9, 0xb5, 0x43, 0xf6, 0x54, 0x5d, 0xa1, 0xf2,
        0xd5, 0x43, 0x29, 0x55, 0x61, 0x3f, 0x0f, 0xcf, 0x62, 0xd4, 0x97, 0x05, 0x24, 0x2a, 0x9a, 0xf9,
        0xe6, 0x1e, 0x85, 0xdc, 0x0d, 0x65, 0x1e, 0x40, 0xdf, 0xcf, 0x01, 0x7b, 0x45, 0x57, 0x58, 0x87,
    };

    // kontrola na predvypoctenou hodnotu
    if(!result_)
        last_error_ = "Memory allocation error.";
    else if(memcmp(key_, scrypt_hash, key_size_) != 0) {
        result_ = false;
        last_error_ = "Resulted value different.";
    }
    return result_;*/

    if(!result_)
        last_error_ = "Memory allocation error.";
    return result_;
}

void scrypt::evaluate(double)
{
    std::cout << "Evaluated:" << std::endl << "  scrypt hash:" << std::showbase << std::internal << std::setfill('0');
    for(size_t i = 0; i < params.scrypt_key_size; i++) {
        if(i % 8 == 0) {
            std::cout << std::endl << "    ";
        }
        std::cout << std::hex << std::setw(4) << static_cast<int>(key_[i]) << std::dec << " ";
    }
    std::cout << std::dec << std::endl;
}

std::string scrypt::get_last_error()
{
    return last_error_;
}

bool scrypt::clear_environment()
{
    delete[] key_;
    key_ = nullptr;
    return true;
}

scrypt::~scrypt()
{
    delete[] key_;
}

//--------------------------------------------
class crypt_tests : public test_suite {
public:
    crypt_tests(void)
    {
        tests.push_back(std::unique_ptr<test>(new aes));
        tests.push_back(std::unique_ptr<test>(new sha256));
        tests.push_back(std::unique_ptr<test>(new scrypt));
    }
    virtual std::string get_name() {return std::string("crypt");}
    virtual std::string get_info() {return std::string("cryptographic algorithms");}
    virtual void print_params()
    {
        std::cout << "   * Params:" << std::endl << "       aes buffer size: " << params.aes_buf_size << "B" << std::endl <<
                     "       aes seed: " << params.aes_seed << std::endl << "       sha256 text: " << params.sha256_text <<
                     std::endl << "       sha256 buf size: " << params.sha256_buf_size << "B" << std::endl << "       sha256 hash size: " <<
                     params.sha256_hash_size << std::endl << "       scrypt N: " << params.scrypt_N << std::endl <<
                     "       scrypt r: " << params.scrypt_r << std::endl << "       scrypt p: " << params.scrypt_p <<
                     std::endl << "       scrypt key size: " << params.scrypt_key_size << std::endl << "       scrypt text: " <<
                     params.scrypt_text << std::endl << "       scrypt salt: " << params.scrypt_salt << std::endl;
    }
    virtual ~crypt_tests() {}
};

extern "C" test_suite* create(bool verbose_flag, Json::Value& root)
{
    verbose = verbose_flag;
    std::string error_message;
    if(!params.init(root, error_message)) {
        std::cout << "Config - " << error_message << std::endl;
        return nullptr;
    }
    return new crypt_tests();
}

#include <string>
#include <sstream>
#include <random>
#include <algorithm>
#include <iostream>

#include "../include/test.h"
#include "../include/json.h"

class config {
public:
    config() : matrix_size(0), seed(0) {}
    bool init(Json::Value& root, std::string& error_message)
    {
        try {
            matrix_size = root.get("size", 512).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'size' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            seed = root.get("seed", 42).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'seed' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        return true;
    }
    size_t matrix_size;
    size_t seed;
};

namespace {
    bool verbose;
    config params;
}


//-----Kontejner matice-----
template <typename T>
class matrix {
public:
    matrix() : data_(nullptr), order_(0) {}
    explicit matrix(size_t order) : data_(new T[order * order]), order_(order) {}
    matrix(size_t order, const T& fill) : matrix<T>(order)
    {
        std::fill_n(data_, order * order, fill);
    }
    matrix(const matrix<T>& src)
    {
        size_t total_size = src.order_ * src.order_;
        data_ = new T[total_size];
        order_ = src.order_;
        std::copy(src.data_, src.data_ + total_size, data_);
    }
    matrix(matrix<T>&& src)
    {
        order_ = src.order_;
        data_ = src.data_;
        src.data_ = nullptr;
        src.order_ = 0;
    }
    ~matrix() {delete[] data_;}
    matrix<T>& operator=(const matrix<T>& right)
    {
        if(right.data_ != this->data_) {
            size_t total_size = right.order_ * right.order_;
            delete[] data_;
            data_ = new T[total_size];
            order_ = right.order_;
            std::copy(right.data_, right.data_ + total_size, data_);
        }
        return *this;
    }
    matrix<T>& operator=(matrix<T>&& right)
    {
        if(right.data_ != this->data_) {
            delete[] data_;
            order_ = right.order_;
            data_ = right.data_;
            right.data_ = nullptr;
            right.order_ = 0;
        }
        return *this;
    }
    matrix<T> operator+(const matrix<T>& right)
    {
        size_t total_size = order_ * order_;
        matrix<T> temp(order_);
        for(size_t i = 0; i < total_size; i++) {
            temp.data_[i] = data_[i] + right.data_[i];
        }
        return temp;
    }
    matrix<T> operator-(const matrix<T>& right)
    {
        size_t total_size = order_ * order_;
        matrix<T> temp(order_);
        for(size_t i = 0; i < total_size; i++) {
            temp.data_[i] = data_[i] - right.data_[i];
        }
        return temp;
    }
    matrix<T> operator*(const matrix<T>& right)
    {
        matrix<T> result(order_, 0);
        for(size_t i = 0; i < order_; i++) {
            for(size_t j= 0; j < order_; j++) {
                T temp = 0;
                for(size_t k = 0; k < order_; k++) {
                    temp += (*this)(i, k) * right(k, j);
                }
                result(i, j) = temp;
            }
        }
        return result;
    }
    T& operator()(size_t row, size_t col)
    {
       return data_[row * order_ + col];
    }
    T const& operator()(size_t row, size_t col) const
    {
       return data_[row * order_ + col];
    }
    matrix<T> upper_left()
    {
        matrix<T> result(order_ / 2);
        for(size_t i = 0; i < order_ / 2; i++) {
            for(size_t j = 0; j < order_ / 2; j++) {
                result(i, j) = this->operator()(i, j);
            }
        }
        return result;
    }
    matrix<T> upper_right()
    {
        matrix<T> result(order_ / 2);
        for(size_t i = 0; i < order_ / 2; i++) {
            for(size_t j = order_ / 2; j < order_; j++) {
                result(i, j - order_ / 2) = this->operator()(i, j);
            }
        }
        return result;
    }
    matrix<T> lower_left()
    {
        matrix<T> result(order_ / 2);
        for(size_t i = order_ / 2; i < order_; i++) {
            for(size_t j = 0; j < order_ / 2; j++) {
                result(i - order_ / 2, j) = this->operator()(i, j);
            }
        }
        return result;
    }
    matrix<T> lower_right()
    {
        matrix<T> result(order_ / 2);
        for(size_t i = order_ / 2; i < order_; i++) {
            for(size_t j = order_ / 2; j < order_; j++) {
                result(i - order_ / 2, j - order_ / 2) = this->operator()(i, j);
            }
        }
        return result;
    }
    size_t order() const
    {
        return order_;
    }

private:
    T* data_;
    size_t order_;
};


//-----Spolecne metody pro maticove testy-----
template <typename T>
class multiply_base : public test {
public:
    multiply_base() : left_(nullptr), right_(nullptr), result_(nullptr) {}
    virtual bool prepare_environment()
    {
        T mean = 1.0 / std::sqrt(params.matrix_size);
        std::default_random_engine generator(params.seed);
        // symetricky interval se stredem v "mean", tedy stredni hodnote tohoto rozdeleni
        std::uniform_real_distribution<T> distribution(0.1 * mean, 1.9 * mean);

        left_ = new matrix<T>(params.matrix_size);
        right_ = new matrix<T>(params.matrix_size);
        result_ = new matrix<T>(params.matrix_size, 0.0);
        for(size_t i = 0; i < params.matrix_size; i++) {
            for(size_t j = 0; j < params.matrix_size; j++) {
                (*left_)(i, j) = distribution(generator);
                (*right_)(i, j) = distribution(generator);
            }
        }
        if(verbose) {
            std::cout << "  matrix size: " << params.matrix_size << " x " << params.matrix_size << std::endl;
            std::cout << "  seed: " << params.seed << std::endl;
        }
/*
        for(int i = 0; i < matrix_size; i++) {
            for(int j = 0; j < matrix_size; j++) {
                std::cout << (*left_)(i, j) << " ";
            }
            std::cout << std::endl;
        }
        for(int i = 0; i < matrix_size; i++) {
            for(int j = 0; j < matrix_size; j++) {
                std::cout << (*right_)(i, j) << " ";
            }
            std::cout << std::endl;
        }
*/
        return true;
    }
    virtual bool check_result()
    {
       /* for(int i = 0; i < matrix_size; i++) {
            for(int j = 0; j < matrix_size; j++) {
                std::cout << (*result_)(i, j) << " ";
            }
            std::cout << std::endl;
        }*/

        T temp;
        for(size_t i = 0; i < params.matrix_size; i++) {
            for(size_t j = 0; j < params.matrix_size; j++) {
                temp = 0;
                for(size_t k = 0; k < params.matrix_size; k++) {
                     temp += (*left_)(i, k) * (*right_)(k, j);
                }
                if((*result_)(i, j) - temp > 0.001) {
                    last_error_ = "Matrix result verification failed.";
                    return false;
                }
            }
        }
        return true;
    }
    virtual void evaluate(double) {}
    virtual std::string get_last_error()
    {
        return last_error_;
    }

    virtual bool clear_environment()
    {
        delete left_;
        delete right_;
        delete result_;
        left_ = right_ = result_ = nullptr;
        return true;
    }

    virtual ~multiply_base()
    {
        delete left_;
        delete right_;
        delete result_;
    }
protected:
    matrix<T>* left_;
    matrix<T>* right_;
    matrix<T>* result_;
    std::string last_error_;
};


//-----Klasicke nasobeni matic-----
template <typename T>
class multiply : public multiply_base<T> {
public:
    virtual std::string get_name()
    {
        std::stringstream name;
        name << "multiply_" << sizeof(T);
        return name.str();
    }
    virtual std::string get_info()
    {
        return "matrix multiplication in O(n^3)";
    }
    virtual void run_test()
    {
        (*result_) = (*left_) * (*right_);
    }
private:
    using multiply_base<T>::left_;
    using multiply_base<T>::right_;
    using multiply_base<T>::result_;
};



//----Hlavni rekurzivni funkce nasobeni podle Strassena-----
template <typename T>
matrix<T> strassen_main(matrix<T>&& left, matrix<T>&& right)
{
    if(left.order() <= 8) {
        return left * right;
    }
    matrix<T> m1 = strassen_main(left.upper_left() + left.lower_right(), right.upper_left() + right.lower_right());
    matrix<T> m2 = strassen_main(left.lower_left() + left.lower_right(), right.upper_left());
    matrix<T> m3 = strassen_main(left.upper_left(), right.upper_right() - right.lower_right());
    matrix<T> m4 = strassen_main(left.lower_right(), right.lower_left() - right.upper_left());
    matrix<T> m5 = strassen_main(left.upper_left() + left.upper_right(), right.lower_right());
    matrix<T> m6 = strassen_main(left.lower_left() - left.upper_left(), right.upper_left() + right.upper_right());
    matrix<T> m7 = strassen_main(left.upper_right() - left.lower_right(), right.lower_left() + right.lower_right());

    matrix<T> c11 = m1 + m4 - m5 + m7;
    matrix<T> c12 = m3 + m5;
    matrix<T> c21 = m2 + m4;
    matrix<T> c22 = m1 - m2 + m3 + m6;

    size_t offset = left.order() / 2;
    for(size_t i = 0; i < offset; i++) {
        for(size_t j = 0; j < offset; j++) {
            left(i, j) = c11(i, j);
            left(i, j + offset) = c12(i, j);
            left(i + offset, j) = c21(i, j);
            left(i + offset, j + offset) = c22(i, j);
        }
    }
    return left;
}


//-----Strassenovo nasobeni matic-----
template <typename T>
class strassen : public multiply_base<T> {
public:
    virtual std::string get_name()
    {
        std::stringstream name;
        name << "strassen_" << sizeof(T);
        return name.str();
    }
    virtual std::string get_info()
    {
        return "matrix multiplication by Strassen algorithm";
    }
    virtual void run_test()
    {
        matrix<T> left(*left_);
        matrix<T> right(*right_);
        (*result_) = strassen_main(std::move(left), std::move(right));
    }
private:
    using multiply_base<T>::left_;
    using multiply_base<T>::right_;
    using multiply_base<T>::result_;
};


//-----Kontejner testu-----
class matrix_tests : public test_suite {
public:
    matrix_tests(void)
    {
        tests.push_back(std::unique_ptr<test>(new multiply<float>));
        tests.push_back(std::unique_ptr<test>(new multiply<double>));
        tests.push_back(std::unique_ptr<test>(new strassen<float>));
        tests.push_back(std::unique_ptr<test>(new strassen<double>));
    }
    virtual std::string get_name() {return std::string("matrix");}
    virtual std::string get_info() {return std::string("floating point calculations");}
    virtual void print_params()
    {
        std::cout << "   * Params:" << std::endl << "       matrix size: " << params.matrix_size << std::endl <<
                     "       seed: " << params.seed << std::endl;
    }
    virtual ~matrix_tests() {}
};



extern "C" test_suite* create(bool verbose_flag, Json::Value& root)
{
    verbose = verbose_flag;
    std::string error_message;
    if(!params.init(root, error_message)) {
        std::cout << "Config - " << error_message << std::endl;
        return nullptr;
    }
    return new matrix_tests();
}

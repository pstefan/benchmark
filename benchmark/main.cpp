#include <dlfcn.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <time.h>
#include <algorithm>
#include <cassert>
#include <dirent.h>
#include <unistd.h>
#include <getopt.h>
#include <sstream>
#include <regex>
#include "../include/test.h"
#include "../include/json.h"

static const char app_title[] = "Benchmark utility, Petr Stefan 2014";

enum return_value : int {OK = 0, BAD_PARAM, LIB_LOAD, CONF_PARSE, BAD_CONF};

// spocteni rozdilu dvou casu reprezentovanych v timespec
timespec tdiff(timespec& start, timespec& end)
{
    timespec temp;
    if(end.tv_nsec < start.tv_nsec) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    }
    else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}

// nacteni jmen modulu z slozky, zkontrolovani podle seznamu include a exclude a vraceni ven pouze jmen, ktera se maji nacist a spustit
int get_libraries(const std::string& dir_name, std::vector<std::string>& files, std::vector<std::string>& exclude, std::vector<std::string>& include)
{
    DIR *dir;
    dirent *dirp;
    dir  = opendir(dir_name.c_str());
    if(dir == NULL) {
        std::cout << "Cannot open directory " << dir_name << " for loading tests." << std::endl;
        return 1;
    }
    dirp = readdir(dir);
    std::regex lib_match("bm_.+\\.so", std::regex::ECMAScript);
    while (dirp != NULL) {
        std::string file(dirp->d_name);
        if(!exclude.empty()) {
            for(const auto& e : exclude) {
                if(file == e) {
                    file = "";
                    break;
                }
            }
        }
        else if(!include.empty()) {
            bool found = false;
            for(const auto& i : include) {
                if(file == i) {
                    found = true;
                    break;
                }
            }
            if(!found)
                file = "";
        }

        if(std::regex_match(file, lib_match))
            files.push_back(dir_name + file);
        dirp = readdir(dir);
    }
    closedir(dir);
    return 0;
}

// nacteni obsahu konfiguracniho souboru a jeho vraceni ve forme stringu
std::string read_config(const std::string& filename, const std::string& program_path)
{
    std::ifstream ifs(filename);

    //pokud je dostupny soubor zadany z prikazove radky
    if(ifs.is_open()) {
        std::cout << "Using config file " << filename << std::endl;
        //nacte cely soubor do stringu
        std::string content((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
        ifs.close();
        return content;
    }
    else {
        //jinak se zkusi pouzit defaultni, umisteny ve stejne slozce jako program
        ifs.open(program_path + "config.json");
        if(ifs.is_open()) {
            std::cout << "Using config file " << program_path << "config.json (default)" << std::endl;
            std::string content((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
            ifs.close();
            return content;
        }
        else {
            //pokud neni dostupny zadny konfiguracni soubor, vratim soubor s prazdnym objektem, tj. u vsech parametru
            //se pouzije defaultni hodnota
            std::cout << "Config file not found. Using default values." << std::endl;
            return "{}";
        }
    }
}

// parsovani nactene konfigurace pomoci knihovny jsoncpp
bool parse_config(Json::Value& root, std::string& config_data, int verbose_flag)
{
    Json::Reader reader;
    bool parse_success = reader.parse(config_data, root);
    if(parse_success) {
        if(verbose_flag == 1) {
            std::cout << "  parsing successful" << std::endl;
        }
        return true;
    }
    else {
        std::cout << "  parsing failed" << std::endl << "  " << reader.getFormattedErrorMessages() << std::endl;
        return false;
    }
}

// spusteni testu (vsechny metody ve spravnem poradi atd.), mereni casu, ...
void run_tests(std::vector<std::unique_ptr<test>>& tests, std::ostream& ostream, int verbose_flag, int times)
{
    clockid_t clockid;
    timespec cpuclk_begin, cpuclk_end, cpuclk_diff;
    clock_getcpuclockid(0, &clockid);
    timespec tbegin, tend, tdifference;
    bool result;

    for(const auto& t : tests) {
        for(int i = 0; i < times; i++) {
            if(verbose_flag == 1)
                ostream << "Test: " << t->get_name() << " - " << t->get_info() << std::endl;
            else {
                ostream << t->get_name() << " ";
                ostream.flush();
            }
            //priprava prostredi pro spusteni testu
            if(verbose_flag == 1)
                ostream << "Preparing environment: " << std::endl;
            result = t->prepare_environment();
            if(verbose_flag == 1) {
                if(result)
                    ostream << "  success" << std::endl;
                else {
                    ostream << "  failure" << std::endl << t->get_last_error() << std::endl << "Quiting..." << std::endl << std::endl;
                    continue;
                }
            }
            else {
                if(!result) {
                    ostream << "- preparing failure: " << t->get_last_error() << std::endl;
                    continue;
                }
            }
            if(verbose_flag == 1)
                ostream << "Testing: " << std::endl;

            // zacatek mereni casu
            clock_gettime(CLOCK_MONOTONIC, &tbegin);
            clock_gettime(clockid, &cpuclk_begin);

            //spusteni testu
            t->run_test();

            // konec mereni casu
            clock_gettime(clockid, &cpuclk_end);
            clock_gettime(CLOCK_MONOTONIC, &tend);

            // spocitani rozdilu casu - doba behu
            tdifference = tdiff(tbegin, tend);
            cpuclk_diff = tdiff(cpuclk_begin, cpuclk_end);

            // vypsani casu
            if(verbose_flag == 1) {
                ostream << "  real time:\t" << tdifference.tv_sec << "." << std::setfill('0') << std::setw(9) <<
                             tdifference.tv_nsec << " seconds" << std::endl;
                ostream << "  CPU time:\t" << cpuclk_diff.tv_sec << "." << std::setfill('0') << std::setw(9) <<
                             cpuclk_diff.tv_nsec << " seconds" << std::endl;
            }
            else {
                ostream << tdifference.tv_sec << "." << std::setfill('0') << std::setw(9) <<
                           tdifference.tv_nsec << " " << cpuclk_diff.tv_sec << "." << std::setfill('0') <<
                           std::setw(9) << cpuclk_diff.tv_nsec << std::endl;
            }

            //kontrola spravnosti vysledku
            if(verbose_flag == 1)
                ostream << "Checking results: " << std::endl;
            result = t->check_result();
            if(verbose_flag == 1) {
                if(result)
                    ostream << "  success" << std::endl;
                else {
                    ostream << "  failure" << std::endl << t->get_last_error() << std::endl;
                }
            }
            else {
                if(!result) {
                    ostream << "- checking failure: " << t->get_last_error() << std::endl;
                }
            }

            //evaluate metoda muze spocitat a vypsat dodatecne informace - napriklad rychlost zapisu dat, cas latence pro jeden paket, ...
            //Vola se jen ve verbose modu pri uspesnem dokonceni testu. Jako parametr se predava namereny real time.
            if(verbose_flag == 1 && result) {
                t->evaluate(tdifference.tv_sec + tdifference.tv_nsec / 1000000000.0);
            }

            //uklid
            if(verbose_flag == 1)
                ostream << "Cleaning environment: " << std::endl;
            result = t->clear_environment();
            if(verbose_flag == 1) {
                if(result)
                    ostream << "  success" << std::endl;
                else {
                    ostream << "  failure" << std::endl << t->get_last_error() << std::endl << "Quiting..." << std::endl << std::endl;
                    break;
                }
                ostream << std::endl;
            }
            else {
                if(!result) {
                    ostream << "- cleaning failure: " << t->get_last_error() << std::endl;
                }
            }
        }
    }
}

// vypis nactenych modulu a obsazenych testu
void print_tests(const std::vector<test_suite*>& tests, const std::vector<std::string>& libraries, int verbose_flag)
{
    int id = 1;
    std::left(std::cout);
    std::cout << std::setfill('-') << std::setw(80) << "-" << std::endl;
    std::cout << std::setfill(' ') << std::setw(55) << "Module" <<
                 std::setw(25) << "File" << std::endl;
    std::cout << std::setfill('-') << std::setw(80) << "-" << std::endl;
    assert(libraries.size() >= tests.size());
    for(const auto& test : tests) {
        std::cout << std::setfill(' ') << std::setw(55) << test->get_name() + " - " + test->get_info() <<
                     std::setw(25) << libraries[id - 1].substr(libraries[id - 1].find_last_of("/") + 1) <<
                     std::endl;
        if(verbose_flag == 1) {
            for(const auto& a : test->tests) {
                std::cout << std::setfill(' ') << "   # " << a->get_name() << " - " << a->get_info() << std::endl;
            }
            test->print_params();
        }
        id++;
    }
    std::cout << std::setfill('-') << std::setw(80) << "-" << std::endl;
}

// rozdeleni parametru prepinacu -i a -e, vraceni seznamu s celymi nazvy pozadovanych modulu
std::vector<std::string>& split(const std::string& s, char delim, std::vector<std::string>& elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back("bm_" + item + ".so");
    }
    return elems;
}

int main(int argc, char** argv)
{
    int verbose_flag = 0;
    int list_flag = 0;
    int all_flag = 0;
    int help_flag = 0;
    int times = 1;
    std::string include;
    std::string exclude;
    std::string config_file;
    struct option long_options[] = {
        {"verbose", no_argument, &verbose_flag, 1},
        {"list", no_argument, &list_flag, 1},
        {"all", no_argument, &all_flag, 1},
        {"help", no_argument, &help_flag, 1},
        {"times", required_argument, 0, 't'},
        {"include", required_argument, 0, 'i'},
        {"exclude", required_argument, 0, 'e'},
        {"config", required_argument, 0, 'c'},
        {0, 0, 0, 0}
    };

    int option_index;
    int opt;
    while(true) {
        opt = getopt_long(argc, argv, "vlat:i:e:c:h", long_options, &option_index);
        if(opt == -1)
            break;
        else if(opt == '?' || opt == ':')
            return return_value::BAD_PARAM;

        switch(opt) {
        case 0:
            ; //tenhle prepinac nastavuje flag
            break;
        case 'v':
            verbose_flag = 1;
            break;
        case 'l':
            list_flag = 1;
            break;
        case 'a':
            all_flag = 1;
            break;
        case 'h':
            help_flag = 1;
            break;
        case 't':
            try {
                times = std::stoi(optarg);
            }
            catch(std::invalid_argument& e) {
                std::cout << "Option -t requires integer argument." << std::endl << "  text of exception: " << e.what() << std::endl;
                return return_value::BAD_PARAM;
            }
            catch(std::out_of_range& e) {
                std::cout << "Option -t argument is out of range. It must fit into int type." << std::endl <<
                             "  text of exception: " << e.what() << std::endl;
                return return_value::BAD_PARAM;
            }
            break;
        case 'i':
            include = optarg;
            break;
        case 'e':
            exclude = optarg;
            break;
        case 'c':
            config_file = optarg;
            break;
        default:
            break;
        }
    }

    //std::cout << verbose_flag << std::endl << list_flag << std::endl << all_flag << std::endl <<
    //             times << std::endl << include << std::endl << exclude << std::endl;

    // prepinace "all", "include" a "exclude" se vylucuji
    bool a = all_flag == 0 ? false : true;
    bool b = include.empty() ? false : true;
    bool c = exclude.empty() ? false : true;
    if((a && b) || (b && c) || (c && a)) {
        std::cout << "Options '--all', '--include' and '--exclude' are mutually exclusive." << std::endl;
        return return_value::BAD_PARAM;
    }

    std::cout << app_title << std::endl;

    //vypis helpu
    if(help_flag == 1) {
        std::cout << std::setiosflags(std::ios::left);
        std::cout << "  " << std::setw(15) << "-h, --help" << "print this help page" << std::endl;
        std::cout << "  " << std::setw(15) << "-v, --verbose" << "set verbose output style" << std::endl;
        std::cout << "  " << std::setw(15) << "-l, --list" << "print list of available tests" << std::endl;
        std::cout << "  " << std::setw(15) << "-t, --times" << "repetitions to each test" << std::endl;
        std::cout << "  " << std::setw(15) << "-a, --all" << "run all available tests (default)" << std::endl;
        std::cout << "  " << std::setw(15) << "-i, --include" << "tests to run" << std::endl;
        std::cout << "  " << std::setw(15) << "-e, --exclude" << "tests not to run" << std::endl;
        std::cout << "  " << std::setw(15) << "-c, --config" << "path to config file" << std::endl;
        return return_value::OK;
    }

    // roztrideni parametru knihoven do jednotlivych retezcu + pridani 'bm_' na zacatek a '.so' na konec
    std::vector<std::string> vect_exclude;
    std::vector<std::string> vect_include;
    split(exclude, ',', vect_exclude);
    split(include, ',', vect_include);


    // zjisteni cesty k binarce tohoto programu
    std::string program_path(argv[0]);
    if(program_path.find_last_of("/") == std::string::npos)
        program_path = "./";
    else
        program_path = program_path.substr(0, program_path.find_last_of("/") + 1);

    //precteni a rozparsovani konfigurace
    std::string config = read_config(config_file, program_path);
    Json::Value root;
    bool parse_success = parse_config(root, config, verbose_flag);
    if(!parse_success) {
        return return_value::CONF_PARSE;
    }

    std::vector<void*> modules; //uchovava handely na otevrene knihovny pomoci dlopen()

    std::vector<std::string> libraries; // nazvy knihoven/modulu k nacteni
    if(get_libraries(program_path, libraries, vect_exclude, vect_include) != 0)
        return return_value::LIB_LOAD;

    int retval = return_value::OK;

    // otevreni knihovny a nacteni sady testu (pomoci create(), ktere je take predana odpovidajici cast konfigurace)
    std::vector<test_suite*> test_suites;
    for(const auto& library : libraries) {
        void* handle = dlopen(library.c_str(), RTLD_NOW);
        if (handle == nullptr)
            std::cout << "Cannot open library '" << library << "': " << dlerror() << std::endl;
        else {
            modules.push_back(handle);
            create_t* create = (create_t*)dlsym(handle, "create");
            if (create == nullptr)
                std::cout << "Failed to link 'create' function in " << library << ": " << dlerror() << std::endl;
            else {
                auto pref_length = library.find_last_of("_") + 1;
                std::string module_name = library.substr(pref_length, library.length() - pref_length - 3);
                auto suite = create(verbose_flag != 0, root[module_name]);
                if(suite != nullptr)
                    test_suites.push_back(suite);
                else {
                    retval = return_value::BAD_CONF;
                    break;
                }
            }
        }
    }

    if (retval == return_value::OK) {
        // jen vypsani testu
        if(list_flag == 1) {
            print_tests(test_suites, libraries, verbose_flag);
        }
        // spusteni testu
        else {
            for(const auto& suite : test_suites) {
                run_tests(suite->tests, std::cout, verbose_flag, times);
            }
        }
    }

    // uvolneni pameti v modulech a zavreni knihoven
    std::for_each(test_suites.begin(), test_suites.end(), [](test_suite* t){t->release();});
    std::for_each(modules.begin(), modules.end(), [](void* module){dlclose(module);});

    return retval;
}


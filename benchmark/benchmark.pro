TEMPLATE = app

DESTDIR = ../debug

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11
QMAKE_CXXFLAGS += -std=c++11
QMAKE_LFLAGS += -Wl,-rpath=\'\$\$ORIGIN\'
LIBS += -ldl

LIBS += -L../debug/ -ljsoncpp

SOURCES += main.cpp

HEADERS += \
    ../include/test.h \
    ../include/json.h


TEMPLATE = lib
TARGET = bm_zlib
CONFIG += plugin no_plugin_name_prefix

DESTDIR = ../debug

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11
QMAKE_CXXFLAGS += -fPIC
QMAKE_LFLAGS += -shared
QMAKE_CXXFLAGS += -std=c++11
QMAKE_LFLAGS += -Wl,-rpath=\'\$\$ORIGIN\'

LIBS += -L../debug/ -ljsoncpp

SOURCES += main.cpp \
    adler32.c \
    compress.c \
    crc32.c \
    deflate.c \
    gzclose.c \
    gzlib.c \
    gzread.c \
    gzwrite.c \
    infback.c \
    inffast.c \
    inflate.c \
    inftrees.c \
    trees.c \
    uncompr.c \
    zutil.c

HEADERS += \
    ../include/test.h \
    ../include/json.h \
    crc32.h \
    deflate.h \
    gzguts.h \
    inffast.h \
    inffixed.h \
    inflate.h \
    inftrees.h \
    trees.h \
    zconf.h \
    zlib.h \
    zutil.h




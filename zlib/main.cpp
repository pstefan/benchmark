#include <string>
#include <vector>
#include <memory>
#include <cstring>
#include <random>
#include <iostream>

#include "zlib.h"
#include "../include/test.h"
#include "../include/json.h"


class config {
public:
    config() : buf_size(0), seed(0) {}
    bool init(Json::Value& root, std::string& error_message)
    {
        try {
            buf_size = root.get("buf_size", 8 * 1024 * 1024).asUInt(); //v Bytech
        }
        catch(std::runtime_error& e) {
            error_message = "'buf_size' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            text = root.get("text", "The quick brown fox jumps over the lazy dog.").asString();
        }
        catch(std::runtime_error& e) {
            error_message = "'text' parameter read failed. String expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            seed = root.get("seed", 42).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'seed' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        return true;
    }
    size_t buf_size;
    std::string text;
    size_t seed;
};

namespace {
    bool verbose;
    config params;
}

class zlib : public test {
public:
    zlib();
    virtual std::string get_name();
    virtual std::string get_info();
    virtual bool prepare_environment();
    virtual void run_test();
    virtual bool check_result();
    virtual void evaluate(double);
    virtual std::string get_last_error();
    virtual bool clear_environment();
    virtual ~zlib();
private:
    std::string last_error_;
    enum result_const {ok, mem_err_c, buf_err_c, mem_err_d, buf_err_d, data_err_d, cmp_err, unknown_err};
    result_const result_;
    uint8_t *buf_src_;
    uint8_t *buf_compress_;
    uint8_t *buf_decompress_;
    uLong compressed_;
};

zlib::zlib() : result_(ok), buf_src_(nullptr), buf_compress_(nullptr), buf_decompress_(nullptr), compressed_(0)
{
}

std::string zlib::get_name()
{
    return std::string("zlib");
}
std::string zlib::get_info()
{
    return std::string("compression with zlib library");
}

bool zlib::prepare_environment()
{
    buf_src_ = new uint8_t[params.buf_size];
    buf_compress_ = new uint8_t[params.buf_size];
    buf_decompress_ = new uint8_t[params.buf_size];
    // bufer naplnim opakovanim textu + prepisu nahodne nektera pismena
    size_t used = 0;
    size_t text_length = params.text.size() + 1;
    while(used + text_length < params.buf_size) {
        memcpy(buf_src_ + used, params.text.c_str(), text_length);
        used += text_length;
    }
    std::minstd_rand0 generator(params.seed);
    for(unsigned i = 0; i < params.buf_size / 32; i++)
        buf_src_[generator() % params.buf_size] = buf_src_[generator() % params.buf_size];
    if(verbose) {
        std::cout << "  data size (uncompressed): " << params.buf_size << "B" << std::endl;
        std::cout << "  buf size: " << params.buf_size << "B" << std::endl;
        std::cout << "  text: " << params.text << std::endl;
        std::cout << "  seed: " << params.seed << std::endl;
    }
    return true;
}

void zlib::run_test()
{
    compressed_ = static_cast<uLong>(params.buf_size);
    uLong decompressed = static_cast<uLong>(params.buf_size);
    // komprese bufferu
    switch(compress(buf_compress_, &compressed_, buf_src_, params.buf_size)) {
    case Z_OK: break;
    case Z_MEM_ERROR: result_ = mem_err_c; return;
    case Z_BUF_ERROR: result_ = buf_err_c; return;
    default: result_ = unknown_err; return;
    };
    // dekomprese
    switch(uncompress(buf_decompress_, &decompressed, buf_compress_, compressed_)) {
    case Z_OK: break;
    case Z_MEM_ERROR: result_ = mem_err_d; return;
    case Z_BUF_ERROR: result_ = buf_err_d; return;
    case Z_DATA_ERROR: result_ = data_err_d; return;
    default: result_ = unknown_err; return;
    }
    result_ = params.buf_size == decompressed ? ok : cmp_err;
}

bool zlib::check_result()
{
    // porovnej pocatek s vysledkem
    if(result_ == ok && memcmp(buf_src_, buf_decompress_, params.buf_size) != 0)
        result_ = cmp_err;
    switch(result_) {
    case ok:
        return true;
    case mem_err_c: last_error_ = "Compression - not enough memory."; break;
    case mem_err_d: last_error_ = "Decompression - not enough memory."; break;
    case buf_err_c: last_error_ = "Compression - small output buffer."; break;
    case buf_err_d: last_error_ = "Decompression - small output buffer."; break;
    case data_err_d: last_error_ = "Decompression - corrupted input data."; break;
    case cmp_err: last_error_ = "Compression/decompression compare failed."; break;
    case unknown_err: last_error_ = "Unknown error."; break;
    }
    return false;
}

void zlib::evaluate(double)
{
    std::cout << "Evaluated:" << std::endl << "  data size (compressed): " << compressed_ << "B" << std::endl;
}

std::string zlib::get_last_error()
{
    return last_error_;
}

bool zlib::clear_environment()
{
    delete [] buf_src_;
    delete [] buf_compress_;
    delete [] buf_decompress_;
    buf_src_ = nullptr;
    buf_compress_ = nullptr;
    buf_decompress_ = nullptr;
    return true;
}

zlib::~zlib()
{
    delete [] buf_src_;
    delete [] buf_compress_;
    delete [] buf_decompress_;
}

//--------------------------------------------

class compress_tests : public test_suite {
public:
    compress_tests(void)
    {
        tests.push_back(std::unique_ptr<test>(new zlib));
    }
    virtual std::string get_name() {return std::string("zlib");}
    virtual std::string get_info() {return std::string("compression algorithms");}
    virtual void print_params()
    {
        std::cout << "   * Params:" << std::endl << "       buf size: " << params.buf_size << "B" << std::endl <<
                     "       seed: " << params.seed << std::endl << "       text: " << params.text << std::endl;
    }
    virtual ~compress_tests() {}
};

extern "C" test_suite* create(bool verbose_flag, Json::Value& root)
{
    verbose = verbose_flag;
    std::string error_message;
    if(!params.init(root, error_message)) {
        std::cout << "Config - " << error_message << std::endl;
        return nullptr;
    }
    return new compress_tests();
}

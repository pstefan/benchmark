####################################################################
# projekt 'benchmark'
####################################################################
#
# Výsledkem sestavení projektu jsou soubor hlavní aplikace, knihov-
# ny pro sady jednotlivých testů a sdilena knihovna jsoncpp pro cteni
# konfiguracnich parametru. Soubory 'benchmark' a knihovny modulu
# (např. 'bm_example.so' pro modul 'example') jsou umístěny v adresáři
# 'DESTDIR'. Zdrojové a hlavičkové soubory pro hlavní aplikaci jsou
# v adresáři 'main', pro jednotlivé moduly v adresáři dle jména modulu.
# Objektové soubory jsou vytvářeny v podadresářích 'DESTDIR' ('main'
# pro hlavní aplikaci a pro moduly dle jejich jména).
#
####################################################################
# konfigurační část
####################################################################
#
# nástroj pro překlad a linkování
#
CC = g++
#
# adresář pro výsledné zkompilované soubory
#
DESTDIR = bin
#
# příznaky pro kompilaci a linkování hlavní aplikace
#
MAIN_CPPFLAGS = -Wall -O3 -std=c++11 -c
MAIN_LFLAGS = -Wl,-rpath='$$ORIGIN'
#
# společné příznaky pro kompilaci a linkování modulů
#
SO_CPPFLAGS = -Wall -O3 -std=c++11 -fPIC -c
SO_CFLAGS = -Wall -O3 -std=c99 -fPIC -xc -c
SO_LFLAGS = -shared -Wl,-rpath='$$ORIGIN'
#
# hlavičkové soubory společné pro hlavní aplikaci i všechny moduly
#
COMMON_HEADERS = include/test.h include/json.h
#
# společné knihovny pro hlavní aplikaci i moduly - v našem případě 
# stačí linkovat pouze k hlavnímu programu
#
COMMON_LIBS = -L$(DESTDIR) -ljsoncpp
#
#------------------------------------------------------
# parametry pro hlavní aplikaci
#------------------------------------------------------
#
# seznam zdrojových souborů v adresáři 'benchmark'
#
MAIN_SRC = main.cpp
#
# seznam hlavičkových souborů v adresáři 'benchmark'
#
MAIN_H =
#
# seznam použitých standartních knihoven
#
MAIN_LIBS = dl rt
#
#------------------------------------------------------
# parametry pro jednotlivé moduly
#------------------------------------------------------
#
# seznam modulů
#
MODULES = mem crypt zlib matrix sort levenshtein net graph card cache join
#
#------------------------------------------------------
# následuje sekce, ve které definujeme pro každý modul
# seznam zdrojových a hlavičkových souborů, dodatečné
# příznaky pro kompilaci a linkování a seznam použitých
# knihoven
#
# např. pro modul 'example' definujeme proměnné
#   example_SRC - zdrojové soubory v adresáři 'example'
#   example_H - hlavičkové soubory v adresáři 'example'
#   example_CPPFLAGS - pro soubory *.cpp
#   example_CFLAGS - pro soubory *.c
#   example_LFLAGS
#   example_LIBS
#------------------------------------------------------
# modul 'mem'
#
mem_SRC = main.cpp
mem_H =
mem_CPPFLAGS =
mem_CFLAGS =
mem_LFLAGS =
mem_LIBS =
#------------------------------------------------------
# modul 'crypt'
#
crypt_SRC = main.cpp aes.c sha256.c scrypt.c
crypt_H = sysendian.h aes.h sha256.h scrypt.h
crypt_CPPFLAGS =
crypt_CFLAGS =
crypt_LFLAGS =
crypt_LIBS =
#------------------------------------------------------
# modul 'zlib'
#
zlib_SRC = main.cpp adler32.c compress.c crc32.c deflate.c gzclose.c gzlib.c gzread.c gzwrite.c
zlib_SRC += infback.c inffast.c inflate.c inftrees.c trees.c uncompr.c zutil.c
zlib_H = crc32.h deflate.h gzguts.h inffixed.h inflate.h inftrees.h trees.h zconf.h zlib.h zutil.h
zlib_CPPFLAGS =
zlib_CFLAGS =
zlib_LFLAGS =
zlib_LIBS =
#------------------------------------------------------
# modul 'matrix'
#
matrix_SRC = main.cpp
matrix_H =
matrix_CPPFLAGS =
matrix_CFLAGS =
matrix_LFLAGS =
matrix_LIBS =
#------------------------------------------------------
# modul 'sort'
#
sort_SRC = main.cpp
sort_H =
sort_CPPFLAGS =
sort_CFLAGS =
sort_LFLAGS =
sort_LIBS =
#------------------------------------------------------
# modul 'levenshtein'
#
levenshtein_SRC = main.cpp
levenshtein_H =
levenshtein_CPPFLAGS =
levenshtein_CFLAGS =
levenshtein_LFLAGS =
levenshtein_LIBS =
#------------------------------------------------------
# modul 'net'
#
net_SRC = main.cpp socket.cpp
net_H = socket.h
net_CPPFLAGS =
net_CFLAGS =
net_LFLAGS =
net_LIBS =
#------------------------------------------------------
# modul 'graph'
#
graph_SRC = main.cpp
graph_H =
graph_CPPFLAGS =
graph_CFLAGS =
graph_LFLAGS =
graph_LIBS =
#------------------------------------------------------
#
# modul 'card'
#
card_SRC = main.cpp
card_H =
card_CPPFLAGS =
card_CFLAGS =
card_LFLAGS =
card_LIBS =
#------------------------------------------------------
#
# modul 'cache'
#
cache_SRC = main.cpp
cache_H =
cache_CPPFLAGS =
cache_CFLAGS =
cache_LFLAGS =
cache_LIBS =
#------------------------------------------------------
#
# modul 'join'
#
join_SRC = main.cpp
join_H =
join_CPPFLAGS =
join_CFLAGS =
join_LFLAGS =
join_LIBS =
#------------------------------------------------------
#
####################################################################
# definice pravidel a závislostí
####################################################################
#
# all - základní cíl pro sestavení aplikace a modulů
#

.PHONY: all

all: $(DESTDIR)/libjsoncpp.so $(DESTDIR)/benchmark $(patsubst %,$(DESTDIR)/bm_%.so,$(MODULES)) | $(DESTDIR)/.

# vytvoření adresáře $(DESTDIR) pokud ještě neexistuje
$(DESTDIR)/.:
	mkdir -p $(DESTDIR)

#------------------------------------------------------
# sestavení hlavní aplikace
#

# vytvoření seznamu objektů
MAIN_OBJS = $(patsubst %.cpp,$(DESTDIR)/main/%.o,$(MAIN_SRC))
# linkování
$(DESTDIR)/benchmark: $(MAIN_OBJS) $(DESTDIR)/libjsoncpp.so
	$(CC) $(MAIN_LFLAGS) -o $@ $(MAIN_OBJS) $(addprefix -l,$(MAIN_LIBS)) $(COMMON_LIBS)
# kontrola existence cílových adresářů (ale bez závislosti na času modifikace)
$(MAIN_OBJS): | $(DESTDIR)/main/.
# vytvoření cílového adresáře pro objektové soubory
$(DESTDIR)/main/.:
	mkdir -p $(DESTDIR)/main
# kompilace jednotlivých souborů
$(DESTDIR)/main/%.o: benchmark/%.cpp $(patsubst %,benchmark/%,$(MAIN_H)) $(COMMON_HEADERS)
	$(CC) $(MAIN_CPPFLAGS) -o $@ $<

#------------------------------------------------------
# sestavení sdílené knihovny libjsoncpp.so
#
# linkování
$(DESTDIR)/libjsoncpp.so: $(DESTDIR)/jsoncpp_lib/main.o
	$(CC) -shared -o $@ $<
# kontrola existence cílového adresáře
$(DESTDIR)/jsoncpp_lib/main.o: | $(DESTDIR)/jsoncpp_lib/.
# vytvoření cílového adresáře pro objektové soubory
$(DESTDIR)/jsoncpp_lib/.:
	mkdir -p $(DESTDIR)/jsoncpp_lib
# vlastní překlad
$(DESTDIR)/jsoncpp_lib/main.o: jsoncpp_lib/main.cpp include/json.h
	$(CC) $(SO_CPPFLAGS) -o $@ $<
	
#------------------------------------------------------
# sestavení modulů probíhá pomocí jednotné šablony
#   parametr - jméno modulu
#

# definice šablony
define module_TEMPLATE
# vytvoření seznamu hlaviček
$(1)_HEADERS = $$(patsubst %,$(1)/%,$$($(1)_H))
# vytvoření seznamu objektů (z C i C++ zdrojových souborů)
$(1)_OBJS = $$(patsubst %.cpp,$$(DESTDIR)/$(1)/%.o,$$(patsubst %.c,$$(DESTDIR)/$(1)/%.o,$$($(1)_SRC)))
# linkování
$$(DESTDIR)/bm_$(1).so: $$($(1)_OBJS) $$(DESTDIR)/libjsoncpp.so
	$$(CC) $$(SO_LFLAGS) $$($(1)_LFLAGS) -o $$@ $$($(1)_OBJS) $$(addprefix -l,$$($(1)_LIBS))
    #$$(CC) $$(SO_LFLAGS) $$($(1)_LFLAGS) -o $$@ $$($(1)_OBJS) $$(addprefix -l,$$($(1)_LIBS)) $$(COMMON_LIBS)
# kontrola existence cílových adresářů (ale bez závislosti na času modifikace)
$$($(1)_OBJS): | $$(DESTDIR)/. $$(DESTDIR)/$(1)/.
# vytvoření cílového adresáře pro objektové soubory
$$(DESTDIR)/$(1)/.:
	mkdir $$(DESTDIR)/$(1)
# kompilace jednotlivých souborů (pomocí C nebo C++ překladače)
$$(DESTDIR)/$(1)/%.o: $(1)/%.c $$($(1)_HEADERS) $$(COMMON_HEADERS)
	$$(CC) $$(SO_CFLAGS) $$($(1)_CFLAGS) -o $$@ $$<
$$(DESTDIR)/$(1)/%.o: $(1)/%.cpp $$($(1)_HEADERS) $$(COMMON_HEADERS)
	$$(CC) $$(SO_CPPFLAGS) $$($(1)_CPPFLAGS) -o $$@ $$<
endef

# vytvoření pravidel pro každý modul pomocí šablony
$(foreach module,$(MODULES),$(eval $(call module_TEMPLATE,$(module))))

####################################################################
#
# clean - vymazání objektových souborů a zachování modulů i binárky hlavního programu
#

.PHONY: clean

clean:
	rm -f $(DESTDIR)/*/*.o

####################################################################
#
# purge - vymazání všech produktů kompilace a linkování
#

.PHONY: purge

purge:
	rm -f $(DESTDIR)/*/*.o
	rm -f $(DESTDIR)/benchmark $(DESTDIR)/bm_*.so $(DESTDIR)/libjsoncpp.so

####################################################################


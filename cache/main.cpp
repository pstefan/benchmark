#include <string>
#include <iostream>
#include <functional>
#include <random>
#include <algorithm>

#include "../include/test.h"
#include "../include/json.h"

class config {
public:
    config() : buffer_size(0), iterations(0) {}
    bool init(Json::Value& root, std::string& error_message)
    {
        try {
            buffer_size = root.get("buffer_size", 134217728).asUInt(); // 128 MB
        }
        catch(std::runtime_error& e) {
            error_message = "'buffer_size' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            iterations = root.get("iterations", 13421772).asUInt(); // precteme kazdy desaty zaznam (buffer_size / 10)
        }
        catch(std::runtime_error& e) {
            error_message = "'iterations' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        return true;
    }
    size_t buffer_size;
    size_t iterations;
};

namespace {
    bool verbose;
    config params;
}


class cache_speed : public test {
public:
    cache_speed() : buffer_(nullptr) {}
    virtual std::string get_info()
    {
        return "read from memory with different usage of cache";
    }
    virtual std::string get_name()
    {
        return "cache";
    }
    virtual bool prepare_environment()
    {
        if(verbose) {
            std::cout << "  buffer_size: " << params.buffer_size << "B" << std::endl;
            std::cout << "  iterations: " << params.iterations << std::endl;
        }
        buf_size_ = params.buffer_size / sizeof(int);
        try {
            buffer_ = new int[buf_size_];
        }
        catch(std::bad_alloc) {
            last_error_ = "failed to allocate memory";
            return false;
        }
        for(size_t i = 0; i < buf_size_; i++) {
            buffer_[i] = 0x55555555;
        }
        return true;
    }
    virtual void run_test()
    {
        int prime = 179425907;
        long long index = 1;
        int val = 0;
        for(size_t i = 0; i < params.iterations; i++) {
            index = (index * prime) % buf_size_;
            val = buffer_[index];
        }
        temp_ = val;
    }
    virtual bool check_result()
    {
        return true;
    }
    virtual void evaluate(double time)
    {
        std::cout << "Evaluated:" << std::endl << "  read speed: " << sizeof(int) * params.iterations / 1024.0 / 1024.0 / time <<
                     " MB/s" << std::endl;
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        delete[] buffer_;
        buffer_ = nullptr;
        return true;
    }
    ~cache_speed()
    {
        delete[] buffer_;
    }
private:
    std::string last_error_;
    int *buffer_;
    size_t buf_size_;
    volatile int temp_;
};


class cache_tests : public test_suite {
public:
    cache_tests(void)
    {
        tests.push_back(std::unique_ptr<test>(new cache_speed));
    }
    virtual std::string get_name() {return std::string("cache");}
    virtual std::string get_info() {return std::string("system cache tests");}
    virtual void print_params()
    {
        std::cout << "   * Params:" << std::endl << "       buffer size: " << params.buffer_size << "B" << std::endl <<
                     "       number of iterations: " << params.iterations << std::endl;
    }
    virtual ~cache_tests() {}
};


extern "C" test_suite* create(bool verbose_flag, Json::Value& root)
{
    verbose = verbose_flag;
    std::string error_message;
    if(!params.init(root, error_message)) {
        std::cout << "Config - " << error_message << std::endl;
        return nullptr;
    }
    return new cache_tests();
}

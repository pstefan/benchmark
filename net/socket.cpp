#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <cstring>
#include <cstdio>
#include "socket.h"

/*
Trida pro zapozdreni socketu a jeho vlastnosti

Sockety jsou pouzity v blokovacim rezimu, pro cteni je nastaven timeout.
Vysilaci a prijimaci buffer jsou nastaveny na hodnotu 64 kB, pro UDP
je vsak i teoreticke maximum pro funkce 'send' a 'recv' o neco mensi.

Z hlediska spojeni funkcionality pro TCP (connection-oriented) a UDP
(connection-less) protokoly je i v pripade UDP pouzita funkce 'connect'
(zapamatuje se adresu, port atd. protilehle strany) a pro manipulaci
s daty pak funkce 'send' a 'recv'. To je pro UDP protokol netypicke,
ale mozne (obvykle je 'connect' vynechat a pouzivat 'sendto' a 'recvfrom').
Vyhodne je i to, ze pripadne datagramy z jinych adres jsou odfiltrovany.

Socket je oteviran s moznosti pouziti IPv4 i IPv6 (je-li implementovano a
povoleno). IPv4 adresy jsou mapovany do IPv6 rozsahu dle normy.
*/

static const int invalid = -1;
const size_t buf_size = 64 * 1024; // 64 kB
const timeval rx_tout = {0L, 500000L}; // sekundy, mikrosekundy

socket_client::socket_client() : socket_(invalid)
{
}

socket_client::~socket_client()
{
    if(socket_ != invalid)
        ::close(socket_);
}

size_t socket_client::buffer_size() const
{
    return buf_size;
}

// otevreni socketu pro komunikaci - urceno nazvem protistrany, portem a protokolem
bool socket_client::open(const char* server, short unsigned port, prot_type type)
{
    if(socket_ != invalid)
        return false; // socket je jiz otevren
    // nalezeni adresy (pripadne vice adres) serveru
    addrinfo hints;
    memset(&hints, 0, sizeof(addrinfo));
    hints.ai_family = AF_UNSPEC; // moznost IPv4 i IPv6
    hints.ai_socktype = static_cast<int>(type); // SOCK_TYPE nebo SOCK_DGRAM
    hints.ai_protocol = 0; // neomezeno
    hints.ai_flags = AI_ADDRCONFIG | AI_V4MAPPED;
    char pport[50];
    addrinfo *address_set;
    sprintf(pport, "%d", port);
    if(getaddrinfo(server, pport, &hints, &address_set) < 0)
        return false;
    // otevreni socketu a navazani spojeni
    // 'getaddrinfo' muze vratit vice moznych adres, procazime je postupne
    // do prvniho uspechu (takovy socket je pouzit pro spojeni)
    for(addrinfo* i = address_set; i != NULL; i = i->ai_next) {
        socket_ = ::socket(i->ai_family, i->ai_socktype, i->ai_protocol);
        if(socket_ != invalid) {
            if(connect(socket_, i->ai_addr, i->ai_addrlen) == 0) {
                // nastaveni socketu (velikost vysilaciho a prijimaciho buferu a timeoutu pro prijem)
                setsockopt(socket_, SOL_SOCKET, SO_RCVBUF, reinterpret_cast<const char *>(&buf_size), sizeof(int));
                setsockopt(socket_, SOL_SOCKET, SO_SNDBUF, reinterpret_cast<const char *>(&buf_size), sizeof(int));
                setsockopt(socket_, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const char *>(&rx_tout), sizeof(timeval));
                break;
            }
            ::close(socket_);
            socket_ = invalid;
        }
    }
    freeaddrinfo(address_set);
    return socket_ != invalid;
}

// odeslani dat po otevrenem socketu
bool socket_client::send(const void* data, size_t length)
{
    if(socket_ == invalid || length > buf_size)
        return false;
    return ::send(socket_, data, length, 0) == static_cast<ssize_t>(length);
}

// prijem dat z otevreneho socketu
// 'received' je velikost skutecne prijatych dat
bool socket_client::receive(void* data, size_t length, size_t* received)
{
    *received = 0;
    if(socket_ == invalid || length > buf_size)
        return false;
    ssize_t rx = ::recv(socket_, data, length, 0);
    if(rx > 0) { // prijata platna data
        *received = static_cast<size_t>(rx);
        return true;
    }
    else // timeout nebo chyba socketu
        return false;
}

// uzavreni socketu
void socket_client::close()
{
    if(socket_ != invalid) {
        ::close(socket_);
        socket_ = invalid;
    }
}

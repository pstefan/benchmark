#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <random>
#include <unistd.h>
#include <time.h>
#include <cstring>
#include <iostream>

#include "../include/test.h"
#include "../include/json.h"
#include "socket.h"


class config {
public:
    config() : test_size(0), ping_count(0), ping_data(0), tcp_port(0), udp_port(0) {}
    bool init(Json::Value& root, std::string& error_message)
    {
        try {
            test_size = root.get("test_size", 8 * 1024 * 1024).asUInt(); //v Bytech
        }
        catch(std::runtime_error& e) {
            error_message = "'test_size' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            ping_count = root.get("ping_count", 4096).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'ping_count' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            ping_data = root.get("ping_data", 64).asUInt(); //v Bytech
        }
        catch(std::runtime_error& e) {
            error_message = "'ping_data' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            server = root.get("server", "localhost").asString();
        }
        catch(std::runtime_error& e) {
            error_message = "'server' parameter read failed. String expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            tcp_port = root.get("tcp_port", 10000).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'tcp_port' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            udp_port = root.get("udp_port", 10000).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'udp_port' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            seed = root.get("seed", 42).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'seed' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        return true;
    }
    size_t test_size;
    size_t ping_count;
    size_t ping_data;
    std::string server;
    size_t tcp_port;
    size_t udp_port;
    size_t seed;
};

namespace {
    bool verbose;
    config params;
}

static const timespec wait_close = {0, 300000000L};

// spolecne vlastnosti sitoveho testu
class net_test : public test {
public:
    net_test(size_t data_size, size_t packet) :
        rx_data_(nullptr), tx_data_(nullptr), data_size_(data_size), packet_(packet), result_(true)
    {
    }
    virtual bool prepare_environment()
    {
        // alokace buferu pro vysilani a prijem dat
        rx_data_ = new uint8_t [data_size_];
        tx_data_ = new uint8_t [data_size_];
        // naplneni vysilaciho buferu nahodnymi daty
        std::minstd_rand0 gen(params.seed);
        std::generate_n(tx_data_, data_size_, gen);
        if(verbose) {
            std::cout << "  data size: " << data_size_ << "B" << std::endl;
            std::cout << "  blocks: " << data_size_ / packet_ << std::endl;
        }
        result_ = true;
        return true;
    }
    virtual void run_test()
    {
        // Ve smycce jsou odesilana a prijimana data po paketech urcene velikosti.
        // Vyjimecne muze nastat situace, kdy prijimaci bufer ma mene dat nez velikost
        // paketu, zbyvajici data jsou nactena pri dalsim cteni. Celkova velikost
        // nactenych dat je kontrolovana a musi odpovidat odeslanym datum.
        size_t sent = 0;
        size_t received = 0;
        size_t rx_now;
        while(received < data_size_) {
            if(sent < data_size_) {
                // predpokladame, ze celkova velikost dat je delitelna velikosti paketu
                if(!client_.send(tx_data_ + sent, packet_)) {
                    result_ = false;
                    last_error_ = "Error sending data to socket.";
                    return;
                }
                sent += packet_;
            }
            if(!client_.receive(rx_data_ + received, packet_, &rx_now)) {
                result_ = false;
                last_error_ = "Error receiving data from socket.";
                return;
            }
            received += rx_now;
        }
    }
    virtual void evaluate(double time)
    {
        std::cout << "Evaluated:" << std::endl << "  speed: " << (data_size_ / 1024 / 1024 * 2 * 8) / time << " Mbit/s" << std::endl;
        //data_size_ je v Bytech, tj. data_size_ / 1024 / 1024 je v MB. *2 je protoze data_size_ Bytu odesilam i prijimam, *8 je prevod
        // na bity. Po vydeleni casem mam pocet megabitu za sekundu.
    }
    virtual bool check_result()
    {
        if(!result_) // test, zda jiz nastala chyba
            return false;
        // kontrola porovnanim vyslanych a prijatych dat
        if(memcmp(rx_data_, tx_data_, data_size_) == 0)
            return true;
        else {
            last_error_ = "Error comparing rx/tx data.";
            return false;
        }
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        // uvolneni pameti
        delete [] rx_data_;
        rx_data_ = nullptr;
        delete [] tx_data_;
        tx_data_ = nullptr;
        // pauza pro spravnou cinnost echo serveru (rozeznani dalsiho testu)
        nanosleep(&wait_close, NULL);
        client_.close();
        return true;
    }
    virtual ~net_test() {delete [] rx_data_; delete [] tx_data_;}
protected:
    socket_client client_;
    uint8_t* rx_data_;
    uint8_t* tx_data_;
    size_t data_size_;
    size_t packet_;
    bool result_;
    std::string last_error_;
};

// specializace pro TCP testy
class tcp_echo : public net_test {
public:
    tcp_echo(size_t data_size, size_t packet) : net_test(data_size, packet) {}
    virtual bool prepare_environment()
    {
        net_test::prepare_environment();
        if(verbose) {
            std::cout << "  server: " << params.server << ":" << params.tcp_port << std::endl;
        }
        if(!client_.open(params.server.c_str(), params.tcp_port, prot_type::tcp)) {
            result_ = false;
            last_error_ = "Error opening socket.";
        }
        return result_;
    }
    virtual std::string get_name()
    {
        std::stringstream name;
        name << "tcp_" << packet_ / 1024;
        return name.str();
    }
    virtual std::string get_info()
    {
        std::stringstream name;
        name << "data echoing on TCP with " << packet_ / 1024 << "KB blocks";
        return name.str();
    }
};

// specializace pro UDP testy
class udp_echo : public net_test {
public:
    udp_echo(size_t data_size, size_t packet) : net_test(data_size, packet) {}
    virtual bool prepare_environment()
    {
        net_test::prepare_environment();
        if(verbose) {
            std::cout << "  server: " << params.server << ":" << params.udp_port << std::endl;
        }
        if(!client_.open(params.server.c_str(), params.udp_port, prot_type::udp)) {
            result_ = false;
            last_error_ = "Error opening socket.";
        }
        return result_;
    }
    virtual std::string get_name()
    {
        std::stringstream name;
        name << "udp_" << packet_ / 1024;
        return name.str();
    }
    virtual std::string get_info()
    {
        std::stringstream name;
        name << "data echoing on UDP with " << packet_ / 1024 << "KB blocks";
        return name.str();
    }
};

// specializace pro mereni latence (kratke bloky po UDP, bez fragmentace)
class latency : public net_test {
public:
    latency(size_t repeats, size_t packet) : net_test(packet * repeats, packet) {}
    virtual bool prepare_environment()
    {
        net_test::prepare_environment();
        if(verbose) {
            std::cout << "  server: " << params.server << ":" << params.udp_port << std::endl;
            std::cout << "  ping count: " << params.ping_count << std::endl;
            std::cout << "  ping data: " << params.ping_data << "B" << std::endl;
        }
        if(!client_.open(params.server.c_str(), params.udp_port, prot_type::udp)) {
            result_ = false;
            last_error_ = "Error opening socket.";
        }
        return result_;
    }
    virtual void evaluate(double time)
    {
        std::cout << "Evaluated:" << std::endl << "  latency: " << time / (data_size_ / packet_) << " seconds" << std::endl; //data_size_ / packet_ je pocet opakovani
    }
    virtual std::string get_name()
    {
        return "latency";
    }
    virtual std::string get_info()
    {
        std::stringstream name;
        name << "data echoing on UDP with " << packet_ << "B blocks";
        return name.str();
    }
};

// kolekce sitovych testu
class net_tests : public test_suite {
public:
    net_tests(void)
    {
        tests.push_back(std::unique_ptr<test>(new latency(params.ping_count, params.ping_data)));
        tests.push_back(std::unique_ptr<test>(new tcp_echo(params.test_size, 2 * 1024)));
        tests.push_back(std::unique_ptr<test>(new udp_echo(params.test_size, 2 * 1024)));
        tests.push_back(std::unique_ptr<test>(new tcp_echo(params.test_size, 8 * 1024)));
        tests.push_back(std::unique_ptr<test>(new udp_echo(params.test_size, 8 * 1024)));
        tests.push_back(std::unique_ptr<test>(new tcp_echo(params.test_size, 32 * 1024)));
        tests.push_back(std::unique_ptr<test>(new udp_echo(params.test_size, 32 * 1024)));
    }
    virtual std::string get_name() {return std::string("net");}
    virtual std::string get_info() {return std::string("network tests");}
    virtual void print_params()
    {
        std::cout << "   * Params:" << std::endl << "       test size: " << params.test_size << "B" << std::endl <<
                     "       ping count: " << params.ping_count << std::endl << "       ping data: " << params.ping_data <<
                     "B" << std::endl << "       server: " << params.server << std::endl << "       tcp port: " <<
                     params.tcp_port << std::endl << "       udp port: " << params.udp_port << std::endl;
    }
    virtual ~net_tests() {}
};

extern "C" test_suite* create(bool verbose_flag, Json::Value& root)
{
    verbose = verbose_flag;
    std::string error_message;
    if(!params.init(root, error_message)) {
        std::cout << "Config - " << error_message << std::endl;
        return nullptr;
    }
    return new net_tests();
}

TEMPLATE = lib
TARGET = bm_net
CONFIG += plugin no_plugin_name_prefix

DESTDIR = ../debug

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11
QMAKE_CXXFLAGS += -fPIC
QMAKE_LFLAGS += -shared
QMAKE_CXXFLAGS += -std=c++11
QMAKE_LFLAGS += -Wl,-rpath=\'\$\$ORIGIN\'

LIBS += -L../debug/ -ljsoncpp

SOURCES += main.cpp socket.cpp

HEADERS += \
    ../include/test.h \
    ../include/json.h \
    socket.h


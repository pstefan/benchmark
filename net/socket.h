#include <sys/socket.h>

enum class prot_type {tcp = SOCK_STREAM, udp = SOCK_DGRAM};

/*
Trida 'socket_client' - zapouzdreni operaci se sockety.
*/
class socket_client {
public:
    socket_client();
    ~socket_client();
    // velikost vysilaciho a prijimaciho buferu
    size_t buffer_size() const;
    // otevreni socketu (pripadne vcetne navazani spojeni)
    bool open(const char* server, short unsigned port, prot_type type);
    // odeslani dat na otevreny socket
    bool send(const void* data, size_t length);
    // prijem dat z otevreneho socketu
    bool receive(void* data, size_t length, size_t* received);
    // uzavreni socketu
    void close();
private:
    int socket_;
};

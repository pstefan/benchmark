#ifndef TEST_H
#define TEST_H

#include <string>
#include <vector>
#include <memory>

#include "json.h"

class test {
public:
    virtual std::string get_name() = 0;
    virtual std::string get_info() = 0;
    virtual bool prepare_environment() = 0;
    virtual void run_test() = 0;
    virtual void evaluate(double time) = 0;
    virtual bool check_result() = 0;
    virtual std::string get_last_error() = 0;
    virtual bool clear_environment() = 0;
    virtual ~test() {}
};


class test_suite {
public:
    std::vector<std::unique_ptr<test>> tests;
    void release()
    {
        delete this;
    }
    virtual std::string get_name() = 0;
    virtual std::string get_info() = 0;
    virtual void print_params() = 0;
    virtual ~test_suite() {}
};

typedef test_suite* create_t(bool, Json::Value&);



#endif // TEST_H

#include <string>
#include <vector>
#include <algorithm>
#include <random>
#include <iostream>

#include "../include/test.h"
#include "../include/json.h"


class config {
public:
    config() : items(0), seed(0) {}
    bool init(Json::Value& root, std::string& error_message)
    {
        try {
            items = root.get("items", 3145728).asUInt(); // 3 * 1024 * 1024
        }
        catch(std::runtime_error& e) {
            error_message = "'items' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            seed = root.get("seed", 42).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'seed' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        return true;
    }
    size_t items;
    size_t seed;
};

namespace {
    bool verbose;
    config params;
}


template <typename T>
class stdsort : public test {
public:
    stdsort() {}
    virtual std::string get_name()
    {
        return std::string("quicksort");
    }
    virtual std::string get_info()
    {
        return std::string("sorting vector of items");
    }
    virtual bool prepare_environment()
    {
        std::minstd_rand0 generator(params.seed);
        data_.reserve(params.items);
        for(size_t i = 0; i < params.items; i++)
            data_.push_back(static_cast<T>(generator()));
        if(verbose) {
            std::cout << "  items: " << params.items << std::endl;
            std::cout << "  item lenght: " << sizeof(T) << std::endl;
            std::cout << "  seed: " << params.seed << std::endl;
        }
        return true;
    }
    virtual void run_test()
    {
        std::sort(data_.begin(), data_.end());
    }
    virtual bool check_result()
    {
        T last = data_[0];
        T current;
        for(size_t i = 1; i < params.items; i++) {
            current = data_[i];
            if(last > current) {
                last_error_ = "Sorting test failed.";
                return false;
            }
            last = current;
        }
        return true;
    }
    virtual void evaluate(double) {}
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        data_.clear();
        return true;
    }
    virtual ~stdsort() {}
private:
    std::string last_error_;
    std::vector<T> data_;
};

class sort_tests : public test_suite {
public:
    sort_tests(void)
    {
        tests.push_back(std::unique_ptr<test>(new stdsort<int>));
    }
    virtual std::string get_name() {return std::string("sort");}
    virtual std::string get_info() {return std::string("sorting data");}
    virtual void print_params()
    {
        std::cout << "   * Params:" << std::endl << "       items: " << params.items << std::endl <<
                     "       seed: " << params.seed << std::endl;
    }
    virtual ~sort_tests() {}
};

extern "C" test_suite* create(bool verbose_flag, Json::Value& root)
{
    verbose = verbose_flag;
    std::string error_message;
    if(!params.init(root, error_message)) {
        std::cout << "Config - " << error_message << std::endl;
        return nullptr;
    }
    return new sort_tests();
}

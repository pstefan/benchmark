#include <string>
#include <sstream>
#include <vector>
#include <memory>
#include <cstdint>
#include <iostream>

#include "../include/test.h"
#include "../include/json.h"


class config {
public:
    config() : memory_size(0), read_loop(0), write_loop(0) {}
    bool init(Json::Value& root, std::string& error_message)
    {
        try {
            memory_size = root.get("memory_size", 128 * 1024 * 1024).asUInt(); //v Bytech
        }
        catch(std::runtime_error& e) {
            error_message = "'memory_size' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            read_loop = root.get("read_loop", 6).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'read_loop' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            write_loop = root.get("write_loop", 6).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'write_loop' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        return true;
    }
    size_t memory_size;
    size_t read_loop;
    size_t write_loop;
};

namespace {
    bool verbose;
    config params;
}


//---sablona tridy cteni z pameti

template<typename T, T pattern>
class read_mem : public test {
public:
    read_mem() : mem_(nullptr), size_(params.memory_size / sizeof(T)), cell_(0) {}
    virtual std::string get_name()
    {
        std::stringstream name;
        name << "read_" << sizeof(T) << "b";
        return name.str();
    }
    virtual std::string get_info()
    {
        std::stringstream name;
        name << "sequential memory read in " << sizeof(T) << "B blocks";
        return name.str();
    }
    virtual bool prepare_environment()
    {
        mem_ = new T[size_];
        for(size_t i = 0; i < size_; i++)
            mem_[i] = pattern;
        if(verbose) {
            std::cout << "  memory size: " << params.memory_size << "B" << std::endl;
            std::cout << "  read loop: " << params.read_loop << std::endl;
        }
        return true;
    }
    virtual void run_test()
    {
        T* mem_ptr;
        for(unsigned j = 0; j != params.read_loop; j++) {
            mem_ptr = mem_;
            for(size_t i = 0; i != size_; i++)
                cell_ = *mem_ptr++;
        }
    }
    virtual bool check_result()
    {
        last_error_.clear();
        if(cell_ != pattern) {
            last_error_ = "Read value is wrong.";
            return false;
        }
        return true;
    }
    virtual void evaluate(double time)
    {
        std::cout << "Evaluated:" << std::endl << "  read speed: " << params.memory_size * params.read_loop / 1024.0 / 1024.0 / time << " MB/s" << std::endl;
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        delete[] mem_;
        mem_ = nullptr;
        return true;
    }
    virtual ~read_mem() { delete[] mem_; }
private:
    T* mem_;
    const size_t size_;
    T cell_;
    std::string last_error_;
};

//--- sablona tridy zapisu do pameti

template<typename T, T pattern>
class write_mem : public test {
public:
    write_mem() : mem_(nullptr), size_(params.memory_size / sizeof(T)) {}
    virtual std::string get_name()
    {
        std::stringstream name;
        name << "write_" << sizeof(T) << "b";
        return name.str();
    }
    virtual std::string get_info()
    {
        std::stringstream name;
        name << "sequential memory write in " << sizeof(T) << "B blocks";
        return name.str();
    }
    virtual bool prepare_environment()
    {
        mem_ = new T[size_];
        if(verbose) {
            std::cout << "  memory size: " << params.memory_size << "B" << std::endl;
            std::cout << "  write loop: " << params.write_loop << std::endl;
        }
        return true;
    }
    virtual void run_test()
    {
        T* mem_ptr;
        for(unsigned j = 0; j != params.write_loop; j++) {
            mem_ptr = mem_;
            for(size_t i = 0; i != size_; i++)
                *mem_ptr++ = pattern;
        }
    }
    virtual bool check_result()
    {
        last_error_.clear();
        if(mem_[0] != pattern || mem_[size_ - 1] != pattern) {
            last_error_ = "Written value is wrong.";
            return false;
        }
        return true;
    }
    virtual void evaluate(double time)
    {
        std::cout << "Evaluated:" << std::endl << "  write speed: " << params.memory_size * params.write_loop / 1024.0 / 1024.0 / time << " MB/s" << std::endl;
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        delete[] mem_;
        mem_ = nullptr;
        return true;
    }
    virtual ~write_mem() { delete[] mem_; }
private:
    T* mem_;
    const size_t size_;
    std::string last_error_;
};

//-------------------------------------------------------

class mem_tests : public test_suite {
public:
    mem_tests(void)
    {
        tests.push_back(std::unique_ptr<test>(new read_mem<uint8_t, UINT8_C(0x55)>));
        tests.push_back(std::unique_ptr<test>(new read_mem<uint16_t, UINT16_C(0x5555)>));
        tests.push_back(std::unique_ptr<test>(new read_mem<uint32_t, UINT32_C(0x55555555)>));
        tests.push_back(std::unique_ptr<test>(new read_mem<uint64_t, UINT64_C(0x5555555555555555)>));
        tests.push_back(std::unique_ptr<test>(new write_mem<uint8_t, UINT8_C(0x55)>));
        tests.push_back(std::unique_ptr<test>(new write_mem<uint16_t, UINT16_C(0x5555)>));
        tests.push_back(std::unique_ptr<test>(new write_mem<uint32_t, UINT32_C(0x55555555)>));
        tests.push_back(std::unique_ptr<test>(new write_mem<uint64_t, UINT64_C(0x5555555555555555)>));
    }
    virtual std::string get_name() {return std::string("mem");}
    virtual std::string get_info() {return std::string("sequential memory access");}
    virtual void print_params()
    {
        std::cout << "   * Params:" << std::endl << "       memory size: " << params.memory_size << "B" << std::endl <<
                     "       read loop: " << params.read_loop << std::endl << "       write loop: " << params.write_loop <<
                     std::endl;
    }
    virtual ~mem_tests() {}
};

extern "C" test_suite* create(bool verbose_flag, Json::Value& root)
{
    verbose = verbose_flag;
    std::string error_message;
    if(!params.init(root, error_message)) {
        std::cout << "Config - " << error_message << std::endl;
        return nullptr;
    }
    return new mem_tests();
}

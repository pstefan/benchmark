#include <string>
#include <iostream>
#include <vector>
#include <random>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>
#include <utility>
#include <tuple>

#include "../include/test.h"
#include "../include/json.h"

class config {
public:
    config() : set1_count(0), set2_count(0), seed(0) {}
    bool init(Json::Value& root, std::string& error_message)
    {
        try {
            set1_count = root.get("set1_count", 1300000).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'set1_count' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            set2_count = root.get("set2_count", 981000).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'set2_count' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            seed = root.get("seed", 42).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'seed' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        return true;
    }
    size_t set1_count;
    size_t set2_count;
    size_t seed;
};

namespace {
    bool verbose;
    config params;
}


/*
 * Pripravi dve mnoziny, kazdou s unikatnimi a nahodnymi daty.
 * Velikost mnoziny 'a' je set1_count, velikost 'b' je set2_count.
 */
void prepare_keys(std::vector<unsigned>& a, std::vector<unsigned>& b)
{
    std::default_random_engine generator(params.seed);
    std::uniform_int_distribution<unsigned> distribution(0, params.set1_count + params.set2_count);
    auto my_random_ = std::bind(distribution, generator);
    std::unordered_set<unsigned> keys;
    keys.reserve(params.set1_count);

    while(keys.size() < params.set1_count) {
        keys.insert(my_random_());
    }

    a.insert(a.begin(), keys.begin(), keys.end());
    keys.clear();

    keys.reserve(params.set2_count);
    while(keys.size() < params.set2_count) {
        keys.insert(my_random_());
    }
    b.insert(b.begin(), keys.begin(), keys.end());
    keys.clear();


  /*  for(const auto& i : a)
        std::cout << i << " ";
    std::cout << std::endl;
    for(const auto& i : b)
        std::cout << i << " ";
    std::cout << std::endl;*/
}

/*
 * Pripravi dve mnoziny dvojic, kazdou s unikatnimi a nahodnymi klici a nahodnymi daty
 * v druhe slozce. Velikost mnoziny 'a' je set1_count, velikost 'b' je set2_count.
 */
void prepare_pairs(std::vector<std::pair<unsigned, unsigned>>& a, std::vector<std::pair<unsigned, unsigned>>& b)
{
    std::default_random_engine generator(params.seed);
    std::uniform_int_distribution<unsigned> distribution(0, params.set1_count + params.set2_count);
    auto my_random_ = std::bind(distribution, generator);
    std::unordered_set<unsigned> keys;
    keys.reserve(params.set1_count);

    while(keys.size() < params.set1_count) {
        keys.insert(my_random_());
    }
    for(const auto i : keys) {
        a.push_back(std::make_pair(i, my_random_()));
    }
    keys.clear();

    keys.reserve(params.set2_count);
    while(keys.size() < params.set2_count) {
        keys.insert(my_random_());
    }
    for(const auto i : keys) {
        b.push_back(std::make_pair(i, my_random_()));
    }
    keys.clear();
}


class merge_1 : public test {
public:
    merge_1() {}
    virtual std::string get_name()
    {
        return "merge_1";
    }
    virtual std::string get_info()
    {
        return "merge join with keys only";
    }
    virtual bool prepare_environment()
    {
        if(verbose) {
            std::cout << "  set 1 items: " << params.set1_count << std::endl;
            std::cout << "  set 2 items: " << params.set2_count << std::endl;
            std::cout << "  seed: " << params.seed << std::endl;
        }
        prepare_keys(a_, b_);
        return true;
    }
    virtual void run_test()
    {
        std::sort(a_.begin(), a_.end());
        std::sort(b_.begin(), b_.end());
        auto it_a = a_.cbegin();
        auto it_b = b_.cbegin();
        while(it_a != a_.cend() && it_b != b_.cend()) {
            if(*it_a == *it_b) {
                result_.push_back(*it_a);
                ++it_a;
                ++it_b;
            }
            else if(*it_a < *it_b) {
                ++it_a;
            }
            else {
                ++it_b;
            }
        }
    }
    virtual bool check_result()
    {
        /*for(const auto& i : result_)
            std::cout << i << " ";
        std::cout << std::endl;*/
        return true;
    }
    virtual void evaluate(double)
    {
        std::cout << "Evaluated: " << std::endl << "  joined items: " << result_.size() << std::endl;
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        a_.clear();
        b_.clear();
        result_.clear();
        return true;
    }
    ~merge_1() {}
private:
    std::string last_error_;
    std::vector<unsigned> a_;
    std::vector<unsigned> b_;
    std::vector<unsigned> result_;
};


class merge_2 : public test {
public:
    merge_2() {}
    virtual std::string get_name()
    {
        return "merge_2";
    }
    virtual std::string get_info()
    {
        return "merge join with key-value pairs";
    }
    virtual bool prepare_environment()
    {
        if(verbose) {
            std::cout << "  set 1 items: " << params.set1_count << std::endl;
            std::cout << "  set 2 items: " << params.set2_count << std::endl;
            std::cout << "  seed: " << params.seed << std::endl;
        }
        prepare_pairs(a_, b_);
        return true;
    }
    virtual void run_test()
    {
        std::sort(a_.begin(), a_.end());
        std::sort(b_.begin(), b_.end());
        auto it_a = a_.cbegin();
        auto it_b = b_.cbegin();
        while(it_a != a_.cend() && it_b != b_.cend()) {
            if(it_a->first == it_b->first) {
                result_.push_back(triple(it_a->first, it_a->second, it_b->second));
                ++it_a;
                ++it_b;
            }
            else if(it_a->first < it_b->first) {
                ++it_a;
            }
            else {
                ++it_b;
            }
        }
    }
    virtual bool check_result()
    {
        /*for(const auto& i : result_)
            std::cout << std::get<0>(i) << "(" << std::get<1>(i) << "," << std::get<2>(i) << ")" << " ";
        std::cout << std::endl;*/
        return true;
    }
    virtual void evaluate(double)
    {
        std::cout << "Evaluated: " << std::endl << "  joined items: " << result_.size() << std::endl;
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        a_.clear();
        b_.clear();
        result_.clear();
        return true;
    }
    ~merge_2() {}
private:
    typedef std::tuple<unsigned, unsigned, unsigned> triple;
    std::string last_error_;
    std::vector<std::pair<unsigned, unsigned>> a_;
    std::vector<std::pair<unsigned, unsigned>> b_;
    std::vector<triple> result_;
};


class hash_1 : public test {
public:
    hash_1() {}
    virtual std::string get_name()
    {
        return "hash_1";
    }
    virtual std::string get_info()
    {
        return "hash join with keys only";
    }
    virtual bool prepare_environment()
    {
        if(verbose) {
            std::cout << "  set 1 items: " << params.set1_count << std::endl;
            std::cout << "  set 2 items: " << params.set2_count << std::endl;
            std::cout << "  seed: " << params.seed << std::endl;
        }
        prepare_keys(a_, b_);
        return true;
    }
    virtual void run_test()
    {
        std::unordered_set<unsigned> hash_set;
        std::vector<unsigned>::const_iterator it;
        std::vector<unsigned>::const_iterator it_end;

        // hashovaci tabulku udelame z mensiho kontejneru
        if(a_.size() >= b_.size()) {
            hash_set.insert(b_.cbegin(), b_.cend());
            it = a_.cbegin();
            it_end = a_.cend();
        }
        else {
            hash_set.insert(a_.cbegin(), a_.cend());
            it = b_.cbegin();
            it_end = b_.cend();
        }

        // pro kazdy prvek z vetsiho kontejneru zkousime najit odpovidajici prvek v hashovane mnozine
        for(; it != it_end; ++it) {
            auto res = hash_set.find(*it);
            if(res != hash_set.end()) {
                result_.push_back(*res);
            }
        }
    }
    virtual bool check_result()
    {
        /*for(const auto& i : result_)
            std::cout << i << " ";
        std::cout << std::endl;*/
        return true;
    }
    virtual void evaluate(double)
    {
        std::cout << "Evaluated: " << std::endl << "  joined items: " << result_.size() << std::endl;
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        a_.clear();
        b_.clear();
        result_.clear();
        return true;
    }
    ~hash_1() {}
private:
    std::string last_error_;
    std::vector<unsigned> a_;
    std::vector<unsigned> b_;
    std::vector<unsigned> result_;
};


class hash_2 : public test {
public:
    hash_2() {}
    virtual std::string get_name()
    {
        return "hash_2";
    }
    virtual std::string get_info()
    {
        return "hash join with key-value pairs";
    }
    virtual bool prepare_environment()
    {
        if(verbose) {
            std::cout << "  set 1 items: " << params.set1_count << std::endl;
            std::cout << "  set 2 items: " << params.set2_count << std::endl;
            std::cout << "  seed: " << params.seed << std::endl;
        }
        prepare_pairs(a_, b_);
        return true;
    }
    virtual void run_test()
    {
        std::unordered_map<unsigned, unsigned> hash_map;

        // hashovaci tabulku udelame z mensiho kontejneru
        // a pro kazdy prvek z vetsiho kontejneru zkousime najit odpovidajici prvek v hashovane mnozine
        if(a_.size() >= b_.size()) {
            hash_map.reserve(b_.size());
            for(const auto& i : b_) {
                hash_map.insert(std::make_pair(i.first, i.second));
            }
            for(const auto& i : a_) {
                auto res = hash_map.find(i.first);
                if(res != hash_map.end()) {
                    result_.push_back(triple(res->first, i.second, res->second));
                }
            }
        }
        else {
            hash_map.reserve(a_.size());
            for(const auto& i : a_) {
                hash_map.insert(std::make_pair(i.first, i.second));
            }
            for(const auto& i : b_) {
                auto res = hash_map.find(i.first);
                if(res != hash_map.end()) {
                    result_.push_back(triple(res->first, res->second, i.second));
                }
            }
        }
    }
    virtual bool check_result()
    {
        /*for(const auto& i : result_)
            std::cout << std::get<0>(i) << "(" << std::get<1>(i) << "," << std::get<2>(i) << ")" << " ";
        std::cout << std::endl;*/
        return true;
    }
    virtual void evaluate(double)
    {
        std::cout << "Evaluated: " << std::endl << "  joined items: " << result_.size() << std::endl;
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        a_.clear();
        b_.clear();
        result_.clear();
        return true;
    }
    ~hash_2() {}
private:
    typedef std::tuple<unsigned, unsigned, unsigned> triple;
    std::string last_error_;
    std::vector<std::pair<unsigned, unsigned>> a_;
    std::vector<std::pair<unsigned, unsigned>> b_;
    std::vector<triple> result_;
};



class join_tests : public test_suite {
public:
    join_tests(void)
    {
        tests.push_back(std::unique_ptr<test>(new merge_1));
        tests.push_back(std::unique_ptr<test>(new merge_2));
        tests.push_back(std::unique_ptr<test>(new hash_1));
        tests.push_back(std::unique_ptr<test>(new hash_2));
    }
    virtual std::string get_name() {return std::string("join");}
    virtual std::string get_info() {return std::string("database inner join operation");}
    virtual void print_params()
    {
        std::cout << "   * Params:" << std::endl << "       set 1 count: " << params.set1_count << std::endl <<
                     "       set 2 count: " << params.set2_count << std::endl << "       seed: " << params.seed <<
                     std::endl;
    }
    virtual ~join_tests() {}
};


extern "C" test_suite* create(bool verbose_flag, Json::Value& root)
{
    verbose = verbose_flag;
    std::string error_message;
    if(!params.init(root, error_message)) {
        std::cout << "Config - " << error_message << std::endl;
        return nullptr;
    }
    return new join_tests();
}


#include <string>
#include <vector>
#include <iostream>

#include "../include/test.h"
#include "../include/json.h"

class config {
public:
    config() {}
    bool init(Json::Value& root, std::string& error_message)
    {
        try {
            first = root.get("first", first_default_).asString();
        }
        catch(std::runtime_error& e) {
            error_message = "'first' parameter read failed. String expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            second = root.get("second", second_default_).asString();
        }
        catch(std::runtime_error& e) {
            error_message = "'second' parameter read failed. String expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        return true;
    }
    std::string first;
    std::string second;
private:
    constexpr static const char* first_default_ = "In information theory and computer science, the Levenshtein distance is a string metric for measuring the difference between two sequences. Informally, the Levenshtein distance between two words is the minimum number of single-character edits (i.e. insertions, deletions or substitutions) required to change one word into the other. The phrase edit distance is often used to refer specifically to Levenshtein distance. It is named after Vladimir Levenshtein, who considered this distance in 1965. It is closely related to pairwise string alignments. Edit distance is usually defined as a parameterizable metric calculated with a specific set of allowed edit operations, and each operation is assigned a cost (possibly infinite). This is further generalized by DNA sequence alignment algorithms such as the Smith–Waterman algorithm, which make an operation's cost depend on where it is applied. Computing the Levenshtein distance is based on the observation that if we reserve a matrix to hold the Levenshtein distances between all prefixes of the first string and all prefixes of the second, then we can compute the values in the matrix in a dynamic programming fashion, and thus find the distance between the two full strings as the last value computed. This proof fails to validate that the number placed in d[i,j] is in fact minimal; this is more difficult to show, and involves an argument by contradiction in which we assume d[i,j] is smaller than the minimum of the three, and use this to show one of the three is not minimal. Note that this implementation does not fit the definition precisely: it always prefers matches, even if insertions or deletions provided a better score. This is equivalent; it can be shown that for every optimal alignment (which induces the Levenshtein distance) there is another optimal alignment that prefers matches in the sense of this implementation. Two examples of the resulting matrix (hovering over a number reveals the operation performed to get that number):Rescue and aid workers in Nepal are struggling to cope with the scale of the devastation dealt by Saturday's powerful earthquake -- digging through rubble by hand, performing surgeries in makeshift operating theaters, scouring notoriously difficult terrain for more victims. But power blackouts in the capital city of Kathmandu, supply shortages and difficulties getting around complicated the efforts. By Tuesday morning, more than 4,400 people were confirmed dead as a result of the earthquake, the overwhelming majority of them in Nepal. Over 8,000 people were reported to have suffered injuries. The United Nations estimated that the disaster had affected 8 million people across the Himalayan nation. More than 1.4 million people are in need of food assistance, the world body said in a situation report Monday. CNN's Dr. Sanjay Gupta said doctors at one Kathmandu hospital had moved patients from the 120-year-old building and into another structure, where they were operating on patients in rooms normally not used as operating theaters. Our Services display some content that is not Google’s. This content is the sole responsibility of the entity that makes it available. We may review content to determine whether it is illegal or violates our policies, and we may remove or refuse to display content that we reasonably believe violates our policies or the law. But that does not necessarily mean that we review content, so please don’t assume that we do.";
    constexpr static const char* second_default_ = "In approximate string matching, the objective is to find matches for short strings in many longer texts, in situations where a small number of differences is to be expected. The short strings could come from a dictionary, for instance. Here, one of the strings is typically short, while the other is arbitrarily long. This has a wide range of applications, for instance, spell checkers, correction systems for optical character recognition, and software to assist natural language translation based on translation memory. The Levenshtein distance can also be computed between two longer strings, but the cost to compute it, which is roughly proportional to the product of the two string lengths, makes this impractical. Thus, when used to aid in fuzzy string searching in applications such as record linkage, the compared strings are usually short to help improve speed of comparisons. It turns out that only two rows of the table are needed for the construction if one does not want to reconstruct the edited input strings (the previous row and the current row being calculated). The invariant maintained throughout the algorithm is that we can transform the initial segment s[1..i] into t[1..j] using a minimum of d[i,j] operations. At the end, the bottom-right element of the array contains the answer. The implementations of the Levenshtein algorithm on this page are illustrative only. Applications will, in most cases, use implementations which use heap allocations sparingly, in particular when large lists of words are compared to each other. The following remarks indicate some of the variations on this and related topics: Most implementations use one- or two-dimensional arrays to store the distances of prefixes of the words compared. In most applications the size of these structures is previously known. This is the case, when, for instance the distance is relevant only if it is below a certain maximally allowed distance (this happens when words are selected from a dictionary to approximately match a given word). In this case the arrays can be preallocated and reused over the various runs of the algorithm over successive words. Some of our Services allow you to upload, submit, store, send or receive content. You retain ownership of any intellectual property rights that you hold in that content. In short, what belongs to you stays yours. When you upload, submit, store, send or receive content to or through our Services, you give Google (and those we work with) a worldwide license to use, host, store, reproduce, modify, create derivative works (such as those resulting from translations, adaptations or other changes we make so that your content works better with our Services), communicate, publish, publicly perform, publicly display and distribute such content. The rights you grant in this license are for the limited purpose of operating, promoting, and improving our Services, and to develop new ones. This license continues even if you stop using our Services (for example, for a business listing you have added to Google Maps). Some Services may offer you ways to access and remove content that has been provided to that Service. Also, in some of our Services, there are terms or settings that narrow the scope of our use of the content submitted in those Services. Make sure you have the necessary rights to grant us this license for any content that you submit to our Services. Our automated systems analyze your content (including emails) to provide you personally relevant product features, such as customized search results, tailored advertising, and spam and malware detection. This analysis occurs as the content is sent, received, and when it is stored. If you have a Google Account, we may display your Profile name, Profile photo, and actions you take on Google or on third-party applications connected to your Google Account (such as +1’s, reviews you write and comments you post) in our Services, including displaying in ads and other commercial contexts. We will respect the choices you make to limit sharing or visibility settings in your Google Account. For example, you can choose your settings so your name and photo do not appear in an ad. We may modify these terms or any additional terms that apply to a Service to, for example, reflect changes to the law or changes to our Services. You should look at the terms regularly. We’ll post notice of modifications to these terms on this page. We’ll post notice of modified additional terms in the applicable Service. Changes will not apply retroactively and will become effective no sooner than fourteen days after they are posted. However, changes addressing new functions for a Service or changes made for legal reasons will be effective immediately. If you do not agree to the modified terms for a Service, you should discontinue your use of that Service. The laws of California, U.S.A., excluding California’s conflict of laws rules, will apply to any disputes arising out of or relating to these terms or the Services. All claims arising out of or relating to these terms or the Services will be litigated exclusively in the federal or state courts of Santa Clara County, California, USA, and you and Google consent to personal jurisdiction in those courts.";
};

namespace {
    bool verbose;
    config params;
}

class levenshtein : public test {
public:
    levenshtein() : distance_(0) {}
    virtual std::string get_name()
    {
        return std::string("levenshtein");
    }
    virtual std::string get_info()
    {
        return std::string("two strings distance");
    }
    virtual bool prepare_environment()
    {
        distance_ = 0;
        if(verbose) {
            std::cout << "  string 1 lenght: " << params.first.length() << std::endl;
            std::cout << "  string 2 lenght: " << params.second.length() << std::endl;
        }
        return true;
    }
    virtual void run_test()
    {
        const size_t len1 = params.first.size();
        const size_t len2 = params.second.size();
        std::vector<std::vector<size_t>> dist(len1 + 1, std::vector<size_t>(len2 + 1));

        dist[0][0] = 0;
        for(size_t i = 1; i <= len1; ++i)
            dist[i][0] = i;
        for(size_t i = 1; i <= len2; ++i)
            dist[0][i] = i;
        for(size_t i = 1; i <= len1; ++i) {
            for(size_t j = 1; j <= len2; ++j) {
                dist[i][j] = std::min(std::min(dist[i - 1][j] + 1, dist[i][j - 1] + 1),
                        dist[i - 1][j - 1] + (params.first[i - 1] == params.second[j - 1] ? 0 : 1));
            }
        }
        distance_ = dist[len1][len2];
    }
    virtual bool check_result()
    {
        return true;
    }
    virtual void evaluate(double)
    {
        std::cout << "Evaluated:" << std::endl << "  distance: " << distance_ << std::endl;
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool clear_environment()
    {
        return true;
    }
    virtual ~levenshtein() {}
private:
    std::string last_error_;
    size_t distance_;
};

//-------------------------------------------------------

class dynamic_programming : public test_suite {
public:
    dynamic_programming(void)
    {
        tests.push_back(std::unique_ptr<test>(new levenshtein));
    }
    virtual std::string get_name() {return std::string("levenshtein");}
    virtual std::string get_info() {return std::string("dynamic programming algorithm");}
    virtual void print_params()
    {
        std::cout << "   * Params:" << std::endl << "       first string length: " << params.first.size() << std::endl <<
                     "       second string length: " << params.second.size() << std::endl;
    }
    virtual ~dynamic_programming() {}
};

extern "C" test_suite* create(bool verbose_flag, Json::Value& root)
{
    verbose = verbose_flag;
    std::string error_message;
    if(!params.init(root, error_message)) {
        std::cout << "Config - " << error_message << std::endl;
        return nullptr;
    }
    return new dynamic_programming();
}

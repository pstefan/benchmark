#include <string>
#include <utility>
#include <random>
#include <algorithm>
#include <limits>
#include <set>
#include <unordered_map>
#include <functional>
#include <iostream>

#include "../include/test.h"
#include "../include/json.h"

class config {
public:
    config() : graph_side(0) {}
    bool init(Json::Value& root, std::string& error_message)
    {
        try {
            graph_side = root.get("side", 512).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'side' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        try {
            seed = root.get("seed", 42).asUInt();
        }
        catch(std::runtime_error& e) {
            error_message = "'seed' parameter read failed. Unsigned int expected.\n  ";
            error_message.append(e.what());
            return false;
        }
        return true;
    }
    size_t graph_side;
    size_t seed;
};


namespace {
    bool verbose;
    config params;
}


class dijkstr_basic : public test {
public:
    dijkstr_basic() {}
    virtual bool prepare_environment()
    {
        std::minstd_rand0 generator(params.seed);
        std::uniform_int_distribution<int> distribution(1,10);
        auto gen = std::bind(distribution, generator);
        size_t vertices_total = params.graph_side * params.graph_side; //graf je rovinny ctvercovy
        vertices_.reserve(vertices_total);
        for(size_t i = 0; i < vertices_total; i++) {
            vertices_.push_back(std::vector<std::pair<size_t, size_t>>());
        }
        size_t edges = 0;
        for(size_t i = 0; i < vertices_total; i++) {
            if(i >= params.graph_side) { // hrana nahoru
                vertices_[i].push_back(std::make_pair(i - params.graph_side, gen()));
                edges++;
            }
            if(i >= params.graph_side && (i + 1) % params.graph_side != 0) { // hrana doprava nahoru
                vertices_[i].push_back(std::make_pair(i - params.graph_side + 1, gen()));
                edges++;
            }
            if((i + 1) % params.graph_side != 0) { // hrana doprava
                vertices_[i].push_back(std::make_pair(i + 1, gen()));
                edges++;
            }
            if(i + params.graph_side < vertices_total) { // hrana dolu
                vertices_[i].push_back(std::make_pair(i + params.graph_side, gen()));
                edges++;
            }
            if(i + params.graph_side < vertices_total && i % params.graph_side != 0) { // hrana doleva dolu
                vertices_[i].push_back(std::make_pair(i + params.graph_side - 1, gen()));
                edges++;
            }
            if(i % params.graph_side != 0) { // hrana doleva
                vertices_[i].push_back(std::make_pair(i - 1, gen()));
                edges++;
            }
        }

        if(verbose) {
            std::cout << "  vertices: " << vertices_total << std::endl;
            std::cout << "  edges: " << edges << std::endl;
            std::cout << "  seed: " << params.seed << std::endl;
            std::cout << "  side (graph side): " << params.graph_side << std::endl;
        }

//        for(size_t a = 0; a < vertices_total; a++) {
//            for(size_t x = 0; x < vertices_[a].size(); x++) {
//               std::cout << "vertex " << a << " to " << vertices_[a][x].first << ": " << vertices_[a][x].second << std::endl;
//            }
//        }

        return true;
    }
    virtual std::string get_last_error()
    {
        return last_error_;
    }
    virtual bool check_result()
    {
        return true;
    }
    virtual void evaluate(double)
    {
        std::cout << "Evaluated:" << std::endl << "  distance: " << result_distance_ << std::endl;
    }
    virtual bool clear_environment()
    {
        vertices_.clear();
        return true;
    }
    virtual ~dijkstr_basic() {}
protected:
    std::string last_error_;
    /*
     * Na indexu i vnejsiho vectoru je seznam jeho sousedu s prislusnymi vahami hran.
     * Seznam je take vector dvojic cisel, kde prvni je cislo vrcholu a druhe je vaha hrany vedouci to toho vrcholu.
     */
    std::vector<std::vector<std::pair<size_t, size_t>>> vertices_;
    size_t result_distance_;
};

namespace dijkstra {
/*
 * Prioritni fronta je implementovana pomoci std::set, 'decrease_key' je provedeno vyjmutim a opetovnym
 * vlozenim. Slozitost O(log n) pro 'push', 'pop' i 'decrease_key', O(1) pro 'top'.
 * Inspirace: http://rosettacode.org/wiki/Dijkstra%27s_algorithm#C.2B.2B
 *
 * Pro rozliseni od std::priority_queue je umisteno na zvlastniho jmenneho prostoru.
 */
/*template <typename T>
class priority_queue {
public:
    typedef std::pair<T, size_t> value_type;
    priority_queue() {}
    ~priority_queue() {}
    void push(T distance, size_t vertex)
    {
        container_.insert(std::make_pair(distance, vertex));
    }
    value_type pop()
    {
        value_type temp = *container_.begin();
        container_.erase(*container_.begin());
        return temp;
    }
    const value_type& top() const
    {
        return *container_.cbegin();
    }
    void decrease_key(size_t vertex, T old_distance, T new_distance)
    {
        container_.erase(std::make_pair(old_distance, vertex));
        container_.insert(std::make_pair(new_distance, vertex));
    }
private:
    // prvni je vaha, druhe je cislo vrcholu, porovnava se v prvni rade podle vahy, v druhe az podle cisla vrcholu
    std::set<value_type> container_;
};*/

/*
 * Implementace prioritni fronty pomoci vazane binarni haldy.
 */
template <typename T>
class priority_queue {
public:
    typedef std::pair<T, size_t> value_type; //vzdalenost - vrchol
    priority_queue() {}
    ~priority_queue() {}
    void push(T distance, size_t vertex) {
        heap_.push_back(std::make_pair(distance, vertex));
        indices_.insert(std::pair<size_t, size_t>(vertex, heap_.size() - 1));
        bubble_up(heap_.size() - 1);
    }
    value_type pop() {
        size_t length = heap_.size();
        auto for_change = indices_.find(heap_[length - 1].second);
        for_change->second = 0; //vrchol co byl posledni ma nyni index v halde 0
        indices_.erase(heap_[0].second);
        value_type top = heap_[0];
        heap_[0] = heap_[length - 1];
        heap_.pop_back();
        bubble_down(0);
        return top;
    }
    const value_type& top() {
        return heap_[0];
    }
    void decrease_key(size_t vertex, T new_distance) {
        size_t vert_index = indices_.find(vertex)->second;
        heap_[vert_index].first = new_distance;
        bubble_up(vert_index); //protoze vzdalenost vzdy zmensujeme, delame pouze bubble_up
    }
private:
    std::vector<value_type> heap_;
    std::unordered_map<size_t, size_t> indices_; // index pro nalezeni konkretniho vrcholu v halde; cislo vrcholu - index v heap_
    void bubble_down(size_t index) {
        size_t length = heap_.size();
        size_t left_child_index = 2 * index + 1;
        size_t right_child_index = 2 * index + 2;

        if(left_child_index >= length)
            return; //index je list

        size_t min_index = index;

        if(heap_[index].first > heap_[left_child_index].first) {
            min_index = left_child_index;
        }
        if((right_child_index < length) && (heap_[min_index].first > heap_[right_child_index].first)) {
            min_index = right_child_index;
        }

        if(min_index != index) {
            //potreba udelat swap
            std::swap(heap_[index], heap_[min_index]);
            std::swap(indices_.find(heap_[index].second)->second, indices_.find(heap_[min_index].second)->second); //u obou vrcholu musime upravit jejich pozici
            bubble_down(min_index);
        }
    }
    void bubble_up(size_t index) {
        if(index == 0)
            return;

        size_t parent_index = (index - 1) / 2;

        if(heap_[parent_index].first > heap_[index].first) {
            std::swap(heap_[parent_index], heap_[index]);
            std::swap(indices_.find(heap_[index].second)->second, indices_.find(heap_[parent_index].second)->second); //u obou vrcholu musime upravit jejich pozici
            bubble_up(parent_index);
        }
    }
};

} // namespace

class dijkstr_n2 : public dijkstr_basic {
public:
    dijkstr_n2() {}
    virtual std::string get_name()
    {
        return std::string("dijk_n2");
    }
    virtual std::string get_info()
    {
        return std::string("Dijkstra shortest path in O(n^2)");
    }
    virtual void run_test()
    {
        const size_t vertices_total = params.graph_side * params.graph_side;
        std::vector<size_t> distance(vertices_total, std::numeric_limits<size_t>::max()); // vsechny vrcholy maji vzdalenost infinity
        std::vector<size_t> prev_vertex(vertices_total); // predchozi vrcholy - slouzi ke zpetnemu zrekonstruovani cesty
        distance[params.graph_side + 1] = 0; // pocatecni vrchol
        size_t vert;
        while(true) {
            size_t local_min = distance[0];
            vert = 0;
            for(size_t i = 1; i < vertices_total; i++) { // vyber nejblizsi vrchol
                if(distance[i] < local_min) {
                    local_min = distance[i];
                    vert = i;
                }
            }
            if(vert == vertices_total - params.graph_side - 2) { // pokud je to koncovy, tak skonci
                break;
            }
            for(const auto i : vertices_[vert]) { // jinak pro vsechny jeho sousedy uprav vzdalenosti
                size_t new_dist = distance[vert] + i.second;
                if(new_dist < distance[i.first]) {
                    distance[i.first] = new_dist;
                    prev_vertex[i.first] = vert;
                }
            }
            distance[vert] = std::numeric_limits<size_t>::max(); // vzdalenost k tomuto vrcholu jiz nepotrebuju - nutno nastavit infinity kvuli dalsimu hledani minima
        }
        result_distance_ = distance[vert];
    }
    virtual ~dijkstr_n2() {}
};

class dijkstr_fast : public dijkstr_basic {
public:
    dijkstr_fast() {}
    virtual std::string get_name()
    {
        return std::string("dijk_fast");
    }
    virtual std::string get_info()
    {
        return std::string("Dijkstra shortest path with priority queue");
    }
    virtual void run_test()
    {
        const size_t vertices_total = params.graph_side * params.graph_side;
        std::vector<size_t> distance(vertices_total, std::numeric_limits<size_t>::max()); // vsechny vrcholy maji vzdalenost infinity
        std::vector<size_t> prev_vertex(vertices_total); // predchozi vrcholy - slouzi ke zpetnemu zrekonstruovani cesty
        distance[params.graph_side + 1] = 0; // pocatecni vrchol
        size_t vert;
        dijkstra::priority_queue<size_t> vert_queue;
        for(size_t i = 0; i < vertices_total; i++) {
            vert_queue.push(distance[i], i);
        }
        while(true) {
            vert = vert_queue.pop().second;
            if(vert == vertices_total - params.graph_side - 2) { // pokud je to koncovy, tak skonci
                break;
            }
            for(const auto i : vertices_[vert]) { // jinak pro vsechny jeho sousedy uprav vzdalenosti
                size_t new_dist = distance[vert] + i.second;
                if(new_dist < distance[i.first]) {
                    vert_queue.decrease_key(i.first, new_dist);
                    distance[i.first] = new_dist;
                    prev_vertex[i.first] = vert;
                }
            }
        }
        result_distance_ = distance[vert];
    }
    virtual ~dijkstr_fast() {}
};


class graph_tests : public test_suite {
public:
    graph_tests(void)
    {
        //tests.push_back(std::unique_ptr<test>(new dijkstr_n2));
        tests.push_back(std::unique_ptr<test>(new dijkstr_fast));
    }
    virtual std::string get_name() {return std::string("graph");}
    virtual std::string get_info() {return std::string("graph algorithms");}
    virtual void print_params()
    {
        std::cout << "   * Params:" << std::endl << "       side (graph side): " << params.graph_side << std::endl <<
                     "       seed: " << params.seed << std::endl;
    }
    virtual ~graph_tests() {}
};

extern "C" test_suite* create(bool verbose_flag, Json::Value& root)
{
    verbose = verbose_flag;
    std::string error_message;
    if(!params.init(root, error_message)) {
        std::cout << "Config - " << error_message << std::endl;
        return nullptr;
    }
    return new graph_tests();
}

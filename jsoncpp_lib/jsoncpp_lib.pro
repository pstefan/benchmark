TEMPLATE = lib

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

TARGET = jsoncpp
DESTDIR = ../debug

CONFIG += c++11

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp

HEADERS += \
    ../include/json.h


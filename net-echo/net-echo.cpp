#include <iostream>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <poll.h>
#include <unistd.h>
#include <errno.h>
#include <cstring>
#include <cstdlib>
#include <getopt.h>

static const int socket_error = -1;
static const int flag_on = 1;
static const int max_connections = 1;
const int buf_size = 64 * 1024; // v bytech
const timeval rx_tout = {0L, 400000L}; // sekundy, mikrosekundy

/*
Sockety jsou pouzity v blokujicim rezimu, pro prijem je nastaven timeout.
*/

// nastaveni velikosti prijimaciho a vysilaciho buferu a prijimaciho timeoutu
bool set_socket(int sock)
{
    if(setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &buf_size, sizeof(int)) < 0)
        return false;
    if(setsockopt(sock, SOL_SOCKET, SO_SNDBUF, &buf_size, sizeof(int)) < 0)
        return false;
    if(setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &rx_tout, sizeof(timeval)) < 0)
        return false;
    return true;
}

// otevreni a konfigurace TCP socketu
int tcp_socket(unsigned short port)
{
    if(port == 0)
        return socket_error;
    // otevreni TCP socketu pro IPv4 i IPv6
    int tcp = socket(AF_INET6, SOCK_STREAM, 0);
    if(tcp < 0)
        return socket_error;
    // povolit vicenasobne pouziti adresy
    if(setsockopt(tcp, SOL_SOCKET, SO_REUSEADDR, &flag_on, sizeof(flag_on)) < 0)
        return socket_error;
    // nastaveni IP adresy (libovolna) a portu
    sockaddr_in6 address;
    memset(&address, 0, sizeof(address));
    address.sin6_family = AF_INET6;
    address.sin6_port = htons(port);
    address.sin6_addr = in6addr_any;
    if(bind(tcp, reinterpret_cast<sockaddr*>(&address), sizeof(address)) < 0)
        return socket_error;
    // nastaveni socketu do naslouchaciho stavu
    if(listen(tcp, max_connections) < 0 || !set_socket(tcp))
        return socket_error;
    // platny socket
    return tcp;
}

// otevreni a konfigurace UDP socketu
int udp_socket(unsigned short port)
{
    if(port == 0)
        return socket_error;
    // otevreni UDP socketu pro IPv4 i IPv6
    int udp = socket(AF_INET6, SOCK_DGRAM, 0);
    if(udp < 0)
        return socket_error;
    // povolit vicenasobne pouziti adresy
    if(setsockopt(udp, SOL_SOCKET, SO_REUSEADDR, &flag_on, sizeof(flag_on)) < 0)
        return socket_error;
    // nastaveni IP adresy (libovolna) a portu
    sockaddr_in6 address;
    memset(&address, 0, sizeof(address));
    address.sin6_family = AF_INET6;
    address.sin6_port = htons(port);
    address.sin6_addr = in6addr_any;
    if(bind(udp, reinterpret_cast<sockaddr*>(&address), sizeof(address)) < 0 || !set_socket(udp))
        return socket_error;
    // platny socket
    return udp;
}

// obsluha pri detekci spojeni po TCP
bool tcp_probe(int tcp, size_t* received, sockaddr_storage* address)
{
    // prijeti spojeni a nastaveni socketu
    socklen_t addr_len = sizeof(sockaddr_storage);
    int sock = accept(tcp, reinterpret_cast<sockaddr*>(address), &addr_len);
    if(sock < 0 || !set_socket(sock))
        return false;
    // prijem dat ze socketu, pri timeoutu ukonceno
    bool result = true;
    char buffer[buf_size];
    *received = 0;
    while(true) {
        // prijem dat
        ssize_t rx_bytes = recv(sock, buffer, sizeof(buffer), 0);
        if(rx_bytes == 0)
            break; // ukonceni pri uzavreni socketu klientem
        else if(rx_bytes < 0) {
            result = errno == EAGAIN || errno == EWOULDBLOCK; // zadna data neprijata
            break;
        }
        *received += rx_bytes;
        // vyslani dat (echo)
        ssize_t tx_bytes = send(sock, buffer, rx_bytes, 0);
        if(tx_bytes == 0)
            break;
        else if(tx_bytes < 0) {
            result = false; // chyba odeslani dat
            break;
        }
    }
    close(sock); // uzavreni pouziteho socketu
    return result;
}

// obsluha pri detekci spojeni po UDP
bool udp_probe(int udp, size_t* received, sockaddr_storage* address)
{
    char buffer[buf_size];
    *received = 0;
    // prijem dat ze socketu, pri timeoutu ukonceno
    // adresa pro odpoved je zjistena z prijateho paketu
    while(true) {
        // prijem dat
        socklen_t addr_len = sizeof(sockaddr_storage);
        ssize_t rx_bytes = recvfrom(udp, buffer, sizeof(buffer), 0, reinterpret_cast<sockaddr*>(address), &addr_len);
        if(rx_bytes < 0)
            return errno == EAGAIN || errno == EWOULDBLOCK; // zadna data neprijata
        *received += rx_bytes;
        // vyslani dat (echo)
        ssize_t tx_bytes = sendto(udp, buffer, rx_bytes, 0, reinterpret_cast<sockaddr*>(address), addr_len);
        if(tx_bytes < 0)
            return false; // chyba odeslani dat
    }
}

// vypsani informaci o protistrane - IP adresa a port
void print_peer(std::ostream& stream, const sockaddr_storage* address)
{
    char buffer[100];
    switch(reinterpret_cast<const sockaddr*>(address)->sa_family) {
    case AF_INET: {
            const sockaddr_in* ipv4 = reinterpret_cast<const sockaddr_in*>(address);
            stream << inet_ntop(AF_INET, &ipv4->sin_addr, buffer, sizeof(buffer));
            stream << ":" << ntohs(ipv4->sin_port);
        }
        break;
    case AF_INET6: {
            const sockaddr_in6* ipv6 = reinterpret_cast<const sockaddr_in6*>(address);
            stream << "[" << inet_ntop(AF_INET6, &ipv6->sin6_addr, buffer, sizeof(buffer)) << "]";
            stream << ":" << ntohs(ipv6->sin6_port);
        }
        break;
    }
}

static const unsigned num_socks = 2;
static const unsigned short tcp_default = 10000;
static const unsigned short udp_default = 10000;
static const char waiting_string[] = "Waiting for connection ... (Ctrl-C to quit)";
static const option opt_table[] {
    {"tcp",        required_argument, 0, 't'},
    {"udp",        required_argument, 0, 'u'},
    {"help",       no_argument,       0, 'h'},
    {nullptr,      0,                 nullptr, 0}
};

/*
Sitove echo pro TCP a UDP - hlavni program

Aplikace je pouze jednovlaknova, pozadavky na zpracovani TCP a UDP prichazeji postupne.
Zjisteni spojeni je reseno pomoci (nekonecne) blokujiciho 'poll'. Preruseni uzivatelem
je mozne pomoci SIGTERM (Ctrl-C z terminalu).

Server nasloucha na vsech dostupnych adresach (loopback i lokalni adresy vsech sitovych
adapteru), porty pro TCP i UDP se konfiguruji z prikazove radky.
*/

int main(int argc, char* argv[])
{
    std::cout << "Network echo for benchmark (TCP/UDP, IPv4/IPv6)" << std::endl;
    // zpracovani argumentu z prikazove radky
    unsigned short tcp_port = tcp_default;
    unsigned short udp_port = udp_default;
    while(true) {
        int option = getopt_long(argc, argv, "t:u:h", opt_table, nullptr);
        if(option == -1)
            break;
        switch(option) {
        case 't': tcp_port = static_cast<unsigned short>(strtoul(optarg, nullptr, 10)); break;
        case 'u': udp_port = static_cast<unsigned short>(strtoul(optarg, nullptr, 10)); break;
        default: // help nebo chyba prepinacu
            std::cout << "  Usage: " << argv[0] << " {options}" << std::endl;
            std::cout << "  Options:" << std::endl;
            std::cout << "    -tNNN,  --tcp=NNN  set TCP port to NNN (default 10000)" << std::endl;
            std::cout << "    -uNNN,  --udp=NNN  set UDP port to NNN (default 10000)" << std::endl;
            std::cout << "    -h,     --help     show this usage page" << std::endl;
            return EXIT_FAILURE;
        }
    }
    // vytvoreni socketu
    int socks[num_socks];
    socks[0] = tcp_socket(tcp_port);
    socks[1] = udp_socket(udp_port);
    // kontrola, zda existuje nejaky platny socket pro prijem spojeni
    bool ok = false;
    if(socks[0] == socket_error)
        std::cout << "  error opening TCP port " << tcp_port << std::endl;
    else {
        std::cout << "  listening on TCP port " << tcp_port << std::endl;
        ok = true;
    }
    if(socks[1] == socket_error)
        std::cout << "  error opening UDP port " << udp_port << std::endl;
    else {
        std::cout << "  listening on UDP port " << udp_port << std::endl;
        ok = true;
    }
    if(!ok)
        return EXIT_FAILURE; // zadny port pro prijem nebyl otevren
    // data pro testovani prichoziho spojeni
    pollfd poll_data[num_socks];
    for(unsigned i = 0; i < num_socks; i++) {
        poll_data[i].fd = socks[i];
        poll_data[i].events = POLLIN;
        poll_data[i].revents = 0;
    }
    // hlavni smycka (prerusi se Ctrl-C)
    std::cout << waiting_string << std::endl;
    while(poll(poll_data, num_socks, -1) > 0) { // blokujici cekani na spojeni
        std::cout << "Connection detected ..." << std::endl;
        size_t received;
        sockaddr_storage address;
        // nalezeni "ziveho" socketu a volani zpracovatelske funkce
        for(unsigned i = 0; i < num_socks; i++) {
            if(poll_data[i].revents == POLLIN) { // socket ma data
                switch(i) {
                case 0: // TCP echo
                    ok = tcp_probe(socks[i], &received, &address);
                    std::cout << "  on TCP port from node ";
                    break;
                case 1: // UDP echo
                    ok = udp_probe(socks[i], &received, &address);
                    std::cout << "  on UDP port from node ";
                    break;
                }
                // vypis vysledku
                print_peer(std::cout, &address);
                std::cout << std::endl;
                if(ok)
                    std::cout << "  " << received << " bytes echoed" << std::endl;
                else
                    std::cout << "  operation failed" << std::endl;
                break;
            }
            else if(poll_data[i].revents != 0) { // chyba na socketu
                std::cout << "  connection failed" << std::endl;
                break;
            }
        }
        // dalsi kolo
        std::cout << waiting_string << std::endl;
    }
    return EXIT_FAILURE;
}
